<?php
    return[
        /*
        |--------------------------------------------------------------------------
        | Permission user
        |--------------------------------------------------------------------------
        |
        */
        'permission' => [
            'create-tasks' =>'Create Tasks',
            'edit-tasks' =>'Edit Tasks',
            'delete-tasks' =>'Delete Tasks',
            'create-users' =>'Create Users',
            'edit-users' =>'Edit User',
            'delete-users' =>'Delete Use',
            'password-users' =>'Reset Password User',
            'create-roles' =>'Create Roles',
            'edit-roles' =>'Edit Roles',
            'delete-roles' =>'Delete Roles',
            'view-logs' =>'Views Log System',
        ],
    ];