$(function () {

    adjustSize();
    handleLinks();


    stikcyHeader();
    leftToRightNavbar();
});
$(document).ready(function () {

    let scrollLink = $('.scroll');
    // Smooth scrolling
    scrollLink.click(function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top-75
        }, 1000 );
    });

    // Active link switching
    $(window).scroll(function() {
        let scrollbarLocation = $(this).scrollTop();
        scrollLink.each(function() {
            let sectionOffset = $(this.hash).offset().top - 20;
            if ( sectionOffset <= scrollbarLocation ) {
                $(this).parent().addClass('active');
                $(this).parent().siblings().removeClass('active');
            }
        })

    });

    $('.arrow-down').click( function () {
        $('.header-top').animate({ marginTop: '0' }, 800);

    });
    $(".video-home").click(function () {
        let html = '<video width="400" controls autoplay id="video-modal"> <source src="'+$(this).data("video")+'"type="video/mp4"></video>';
        $('.show-video').html(html)
    });

    $('.btn-rounded').click( function () {
        var ban_video = document.getElementById("video-modal");
        ban_video.currentTime = 0;
        ban_video.pause();
    });

    $('.main').click( function () {
        $('.credits-content,.close_credit').slideToggle();
        $(this).hide();
    });
    $('.close_credit').click( function () {
        $('.credits-content,.close_credit').slideToggle();
        $('.main').show();
    });


    $(".reel-grid").css({
        "background-image": "url({{asset('img/page/sl2.jpg')}})",
    });
    $('.ytb-close').click(function () {
        $('.popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    $(".house").click(function () {
        let html = '  <iframe class="popup-youtube-player" width="640" height="360" src="http://www.youtube.com/embed/' + $(this).data("modal") + '?enablejsapi=1&version=3&playerapiid=ytplayer"' +
            ' frameborder="0" allowfullscreen="true" allowscriptaccess="always"></iframe>';
        $('.embed-responsive').html(html)
    });
    $(".house").hover(function () {
        $this = $(this);
        $('.ytb-close').click(function () {
            $('.popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        })
        if ($(".house:hover").length == 1) {
            $(".reel-grid").css({
                "background-image": "url(" + $(this).data("bg") + ")",
            });
            $(this).addClass('color1');
        } else {
            $('.color-active').hide();
            $(this).show();
        }
    });
    $(".color").hover(function () {

        if ($(".color:hover").length == 1) {
            $('.color-active').show();
            $(this).hide();

        } else {
            $('.color-active').hide();
            $(this).show();
        }

    });
// document ready

});

function stikcyHeader() {

    var $win = $(window),
        $main = $('main'),
        $nav = $('nav'),
        navHeight = $nav.outerHeight(),
        footerHeight = $('footer').outerHeight(),
        docmentHeight = $(document).height(),
        navPos = $nav.offset().top,
        fixedClass = 'is-fixed',
        hideClass = 'is-hide';

    $win.on('load scroll', function () {
        var value = $(this).scrollTop(),
            scrollPos = $win.height() + value;

        if (value > navPos) {
            if (docmentHeight - scrollPos <= footerHeight) {
                $nav.addClass(hideClass);
            } else {
                $nav.removeClass(hideClass);
            }
            $nav.addClass(fixedClass);
            $(".header-top").css('margin-top', '0');
            $main.css('margin-top', navHeight);
        } else {
            $nav.removeClass(fixedClass);
            $main.css('margin-top', '0');
        }
    });
}



function adjustSize() {

    let headerHeight = $("#header-wrapper").outerHeight();

    $("#main-content-wrapper").css({
        /*paddingTop: (headerHeight -1) + "px",*/
        minHeight: getMainContentMinHeight() + "px"
    });

    // Scroll to top when scroll-to-top arrow is clicked
    $('#back_to_top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 500);
    });
}

function handleLinks() {
    $(".link-page-9").click(function () {
        goToPage("page9.html");
    });

    $(".go-home").click(function () {
        goToPage("index.html");
    });

}

function openPage(url) {
    window.open(url, "_blank");
}

function goToPage(url) {
    location.href = url;
}

function getMainContentMinHeight() {

    let screenHeight = $(window).innerHeight();
    let headerHeight = $("#header-wrapper").outerHeight();
    let footerHeight = $("#footer-wrapper").outerHeight();
    let paddingBottom = 10;

    let mainContentMinHeight = screenHeight - headerHeight - footerHeight - paddingBottom;

    return mainContentMinHeight;
}
//show image file
function showimg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $("#nnavatar").show();
        $(".new-av").hide();
        reader.onload = function (e) {
            $('#nnavatar')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$('#chooseFile').bind('change', function () {

    let filename = $("#chooseFile").val();
    $(".file-select-name").show();
    if (/^\s*$/.test(filename)) {

        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
    }
    else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
    }
});

/**
 * Closing an open collapsed navbar when clicking outside
 */
function leftToRightNavbar() {

    $('#nav-icon1').on('click', function() {

        $navMenuCont = $($(this).data('target'));

        $navMenuCont.animate({
            'width': 'toggle'
        }, 350);

        $(".menu-overlay").fadeToggle(500);
        $("#nav-icon1").toggleClass('open');
        $("#top_navbar").fadeToggle();
    });
    $(".menu-overlay").click(function(event) {
        $("button.navbar-toggler").trigger("click");
        $(".menu-overlay").fadeOut(500);
    });

}
