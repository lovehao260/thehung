$(function () {

    adjustSize();
    handleLinks();

    $('.content-wrapper').hide().fadeIn(1500);
    $('footer').hide().fadeIn(1500);
    leftToRightNavbar();
    /* For stikcy header */
    stikcyHeader();

});
$(document).ready(function () {

    let scrollLink = $('.scroll');
    // Smooth scrolling
    scrollLink.click(function(e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: $(this.hash).offset().top-75
        }, 1000 );
    });
    $('.arrow-down').click( function () {
        $('.header-top').animate({ marginTop: '0' }, 800);

    });


    $('.main').click( function () {
        $('.credits-content,.close_credit').slideToggle();
        $(this).hide();
    });
    $('.close_credit').click( function () {
        $('.credits-content,.close_credit').slideToggle();
        $('.main').show();
    });
    $('.btn-rounded').click( function () {
        $('.popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');

    })

// document ready
});

function stikcyHeader() {

    var distance = $('.list-content').offset().top,
        $window = $(window);

    $window.scroll(function() {

        $window.scrollTop() <= 880 ?   $('.header-detail #header-wrapper').css('top','20'):     $('.header-detail #header-wrapper').css('top','0')

    });
}



function adjustSize() {

    let headerHeight = $("#header-wrapper").outerHeight();

    $("#main-content-wrapper").css({
        /*paddingTop: (headerHeight -1) + "px",*/
        minHeight: getMainContentMinHeight() + "px"
    });

    // Scroll to top when scroll-to-top arrow is clicked
    $('#back_to_top').click(function () {
        $('body, html').animate({
            scrollTop: 0
        }, 500);
    });
}

function handleLinks() {
    $(".link-page-9").click(function () {
        goToPage("page9.html");
    });

    $(".go-home").click(function () {
        goToPage("index.html");
    });

}

function openPage(url) {
    window.open(url, "_blank");
}

function goToPage(url) {
    location.href = url;
}

function getMainContentMinHeight() {

    let screenHeight = $(window).innerHeight();
    let headerHeight = $("#header-wrapper").outerHeight();
    let footerHeight = $("#footer-wrapper").outerHeight();
    let paddingBottom = 10;

    let mainContentMinHeight = screenHeight - headerHeight - footerHeight - paddingBottom;

    return mainContentMinHeight;
}



/**
 * Closing an open collapsed navbar when clicking outside
 */
function leftToRightNavbar() {

    $('#nav-icon1').on('click', function() {

        $navMenuCont = $($(this).data('target'));

        $navMenuCont.animate({
            'width': 'toggle'
        }, 350);

        $(".menu-overlay").fadeToggle(500);
        $("#nav-icon1").toggleClass('open');
        $("#top_navbar").fadeToggle();
    });
    $(".menu-overlay").click(function(event) {
        $("button.navbar-toggler").trigger("click");
        $(".menu-overlay").fadeOut(500);
    });

}
