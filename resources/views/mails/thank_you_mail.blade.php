<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
    <meta name="viewport" content="width=600,initial-scale = 2.3,user-scalable=no">
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700' rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Quicksand:300,400,700' rel="stylesheet">
    <!--<![endif]-->


    <style type="text/css">
        body {
            width: 100%;
            background-color: #ffffff;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            mso-margin-top-alt: 0px;
            mso-margin-bottom-alt: 0px;
            mso-padding-alt: 0px 0px 0px 0px;
        }

        p,
        h1,
        h2,
        h3,
        h4 {
            margin-top: 0;
            margin-bottom: 0;
            padding-top: 0;
            padding-bottom: 0;
        }

        span.preheader {
            display: none;
            font-size: 1px;
        }

        html {
            width: 100%;
        }

        table {
            font-size: 14px;
            border: 0;
        }

        /* ----------- responsivity ----------- */

        @media only screen and (max-width: 640px) {
            /*------ top header ------ */
            .main-header {
                font-size: 20px !important;
            }

            .main-section-header {
                font-size: 28px !important;
            }

            .show {
                display: block !important;
            }

            .hide {
                display: none !important;
            }

            .align-center {
                text-align: center !important;
            }

            .no-bg {
                background: none !important;
            }

            /*----- main image -------*/
            .main-image img {
                width: 440px !important;
                height: auto !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 440px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 100% !important;

            }

            .main-button {
                width: 220px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 320px !important;
                height: auto !important;
            }

            .team-img img {
                width: 100% !important;
                height: auto !important;
            }
        }

        @media only screen and (max-width: 479px) {
            /*------ top header ------ */
            .main-header {
                font-size: 18px !important;
            }

            .main-section-header {
                font-size: 26px !important;
            }

            /* ====== divider ====== */
            .divider img {
                width: 280px !important;
            }

            /*-------- container --------*/
            .container590 {
                width: 100% !important;
                margin: auto;
            }


            .container580 {
                width: 260px !important;
            }

            /*-------- secions ----------*/
            .section-img img {
                width: 280px !important;
                height: auto !important;
            }
        }
    </style>
    <!--[if gte mso 9]>
    <style type=”text/css”>
        body {
            font-family: arial, sans-serif !important;
        }
    </style>
    <![endif]-->
</head>


<body class="respond" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- pre-header -->
<table style="display:none!important;">
    <tr>
        <td>
            <div style="overflow:hidden;display:none;font-size:1px;color:#ffffff;line-height:1px;font-family:Arial;maxheight:0px;max-width:0px;opacity:0;">
                Welcome to {{$company->name}}
            </div>
        </td>
    </tr>
</table>
<!-- pre-header end -->
<!-- header -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff">

    <tr>
        <td align="center">
            <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590">
                <tr>
                    <td align="center">

                        <table border="0" align="center" width="590" cellpadding="0" cellspacing="0"
                               class="container590">

                            <tr>
                                <td align="center" height="70" style="height:70px;">
                                    <a href=""
                                       style="display: block; border-style: none !important; border: 0 !important;">
                                              <img
                                                width="100" border="0" style="display: block; width: 200px;"
                                                src="https://yaviet.com/wp-content/uploads/2018/07/LOGO-YAVIET-web.png" alt="logo"/>
                                        {{--                                                src="https://reviewsnice.com/sakura/img/page/logo.png" alt="logo"/>--}}
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>
</table>
<!-- end header -->

<!-- big image section -->
@if(preg_match('/[\x{4E00}-\x{9FBF}]/u', $content['message']) > 0 ||preg_match('/^[ぁ-ん]+$/u', $content['message']) > 0)
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">
        <tr>
            <td align="center">
                <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590">
                    <tr>
                        <td align="center"
                            style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;"
                            class="main-header">
                            <!-- section text ======-->
                            <div style="line-height: 35px">
                                へようこそ <span style="color: #5caad2;">{{$company->name}}</span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="40" align="center" cellpadding="0" cellspacing="0"
                                   bgcolor="eeeeee">
                                <tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="left">
                            <table border="0" width="600" align="center" cellpadding="0" cellspacing="0"
                                   class="container590">
                                <tr>
                                    <td align="left"
                                        style="color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <!-- section text ======-->

                                        <p style="line-height: 24px; margin-bottom:15px">
                                            親愛な: <span
                                                    style="color: #0b2e13;font-weight: 600">{{$content['name']}}</span>
                                        </p>
                                        <p style="line-height: 24px;margin-bottom:15px;">
                                            あなたの質問はサポートする必要があります :"<span
                                                    style="font-weight: 500">{{$content['message']}}</span> "
                                        </p>
                                        <p style="line-height: 24px; margin-bottom:20px;">
                                            {{$company->mail_footer_jp}}
                                        </p>
                                        <table border="0" align="center" width="180" cellpadding="0" cellspacing="0"
                                               bgcolor="5caad2" style="margin-bottom:20px;">

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center"
                                                    style="color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;">
                                                    <!-- main section button -->

                                                    <div style="line-height: 22px;">
                                                        <a href="{{url('/')}}"
                                                           style="color: #ffffff; text-decoration: none;">ウェブサイト</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                        </table>
                                        <p style="line-height: 24px">
                                            愛,
                                        </p>
                                        <p style="line-height: 24px">
                                            {{$company->code}} チーム
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        <tr>
            <td height="20" style="font-size: 40px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>
@else
    <table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">

        <tr>
            <td align="center">
                <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590">
                    <tr>
                        <td align="center"
                            style="color: #343434; font-size: 24px; font-family: Quicksand, Calibri, sans-serif; font-weight:700;letter-spacing: 3px; line-height: 35px;"
                            class="main-header">
                            <!-- section text ======-->

                            <div style="line-height: 35px">
                                Welcome to the <span style="color: #5caad2;">{{$company->name}}</span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="center">
                            <table border="0" width="40" align="center" cellpadding="0" cellspacing="0"
                                   bgcolor="eeeeee">
                                <tr>
                                    <td height="2" style="font-size: 2px; line-height: 2px;">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="20" style="font-size: 20px; line-height: 20px;">&nbsp;</td>
                    </tr>

                    <tr>
                        <td align="left">
                            <table border="0" width="600" align="center" cellpadding="0" cellspacing="0"
                                   class="container590">
                                <tr>
                                    <td align="left"
                                        style="color: #888888; font-size: 16px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                        <!-- section text ======-->

                                        <p style="line-height: 24px; margin-bottom:15px">
                                            Dear: <span
                                                    style="color: #0b2e13;font-weight: 600">{{$content['name']}}</span>

                                        </p>
                                        <p style="line-height: 24px; margin-bottom:20px;">
                                            {{$company->mail_footer_en}}
                                        </p>
                                        <p style="line-height: 24px;margin-bottom:15px;">
                                            Your question need to support :"<span
                                                    style="font-weight: 500">{{$content['message']}}</span> "
                                        </p>
                                        <table border="0" align="center" width="180" cellpadding="0" cellspacing="0"
                                               bgcolor="5caad2" style="margin-bottom:20px;">

                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="center"
                                                    style="color: #ffffff; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 22px; letter-spacing: 2px;">
                                                    <!-- main section button -->
                                                    <div style="line-height: 22px;">
                                                        <a href="{{$url_base}}"
                                                           style="color: #ffffff; text-decoration: none;">MY WEBSITE</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10" style="font-size: 10px; line-height: 10px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <p style="line-height: 24px">
                                            Love,
                                        </p>
                                        <p style="line-height: 24px">
                                            {{$company->code}} Team
                                        </p>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>

        <tr>
            <td height="20" style="font-size: 40px; line-height: 20px;">&nbsp;</td>
        </tr>

    </table>
@endif


<!-- contact section -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="ffffff" class="bg_color">


    <tr>
        <td align="center">
            <table border="0" align="center" width="600" cellpadding="0" cellspacing="0" class="container590 bg_color">

                <tr>
                    <td align="center">
                        <table border="0" align="center" width="610" cellpadding="0" cellspacing="0"
                               class="container590 bg_color">
                            <tr>
                                <!-- logo -->
                                <td align="left">
                                    <a href="{{$url_base}}"
                                       style="display: block; border-style: none !important; border: 0 !important;">
                                        <img width="80" border="0" style="display: block; width: 150px;" src="{{$logo}}" alt=""/>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    @foreach($company_contact as $item)
                                        <table border="0" width="300" align="left" cellpadding="0" cellspacing="0"
                                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                                               class="container590">
                                            <tr>
                                                <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
                                            </tr>

                                            <tr>
                                                <td align="left"
                                                    style="color: #888888; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 23px;"
                                                    class="text_color">
                                                    <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">

                                                        Head-{{$stt++}}: <br/>
                                                        <p>Address:
                                                            <span style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">
                                                           {{$item->address}}</span>
                                                        </p>

                                                    </div>
                                                    <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">
                                                        <p>Email:
                                                            <a href="mailto:"
                                                               style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">{{$item->mail}}</a>
                                                        </p>
                                                    </div>
                                                    <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">
                                                        <p>Tel:
                                                            <span
                                                                    style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">{{$item->phone}} ||
                                                        </span>
                                                            Fax:
                                                            <span
                                                                    style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">{{$item->fax}}
                                                        </span>
                                                        </p>

                                                    </div>
                                                    <div style="color: #333333; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; font-weight: 600; mso-line-height-rule: exactly; line-height: 23px;">
                                                        <p>Hotline:
                                                            <span
                                                                    style="color: #888888; font-size: 14px; font-family: 'Hind Siliguri', Calibri, Sans-serif; font-weight: 400;">{{$item->hotline}}</span>
                                                        </p>
                                                    </div>
                                                </td>
                                            </tr>

                                        </table>
                                @endforeach
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>

        <table border="0" width="100%" cellpadding="0" cellspacing="0"
               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
               class="container590">

            <tr>
                <td class="hide" height="25" style="font-size: 45px; line-height: 25px;">&nbsp;</td>
            </tr>

            <tr>
                <td>
                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <a href="{{$url_base}}"
                                   style="display: block; border-style: none !important; border: 0 !important;"><img
                                            width="34" border="0" style="display: block;"
                                            src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/youtube-icon_32x32.png"
                                            alt=""></a>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <a href="https://youtube.com"
                                   style="display: block; border-style: none !important; border: 0 !important;"><img
                                            width="34" border="0" style="display: block;"
                                            src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/youtube-icon_32x32.png"
                                            alt=""></a>
                            </td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <a href="https://youtube.com"
                                   style="display: block; border-style: none !important; border: 0 !important;"><img
                                            width="34" border="0" style="display: block;"
                                            src="https://68ef2f69c7787d4078ac-7864ae55ba174c40683f10ab811d9167.ssl.cf1.rackcdn.com/skype-icon_32x32.png"
                                            alt=""></a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="15" style="font-size: 15px; line-height: 15px;">&nbsp;</td>
            </tr>
        </table>
    </tr>

</table>
<!-- end section -->

<!-- footer ====== -->
<table border="0" width="100%" cellpadding="0" cellspacing="0" bgcolor="f4f4f4">

    <tr>
        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
    </tr>

    <tr>
        <td align="center">
            <table border="0" align="center" width="610" cellpadding="0" cellspacing="0" class="container590">
                <tr>
                    <td>
                        <table border="0" align="center" cellpadding="0" cellspacing="0"
                               style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"
                               class="container590">
                            <tr>
                                <td align="center"
                                    style="color: #aaaaaa; font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;">
                                    <div style="line-height: 24px;">
                                        <div style="color: #333333;">Copyright © 2011-2019 <p>
                                                <a style="font-size: 14px; font-family: 'Work Sans', Calibri, sans-serif; line-height: 24px;color: #5caad2; text-decoration: none;font-weight:bold;"
                                                   href="{{url('/')}}"> {{$company->name}}.</a>- All rights reserved.
                                            </p>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </td>
    </tr>

    <tr>
        <td height="25" style="font-size: 25px; line-height: 25px;">&nbsp;</td>
    </tr>

</table>
<!-- end footer ====== -->

</body>

</html>