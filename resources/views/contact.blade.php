@extends('layouts.app')
@section('content')

    <header class="header" style="margin-top:0">
        <div id="header-wrapper" class=" header-wrapper" role="main">
            <!-- Top navigation bar -->
            <div class=" row-header">
                <!-- navbar -->
                <nav>
                    <div class="row menu-header animated fadeInDown">
                        <div class="col-lg-3 col-12 icon-logo">
                            <!-- Toggle menu button -->
                            <div data-toggle="slide-collapse" data-target="#top_navbar">
                                <button type="button" class="btn navbar-toggler main-icon">
                                    <div id="nav-icon1">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </button>
                            </div>
                            <div class="logo-menu">
                                <a href="{{url('/')}}" class="logo">
                                    <img src="{{asset('img/page/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End btn office tours -->
                        <div class="col-lg-9 col-sm-12" id="top_navbar">
                            <ul class="menu">
                                <li><a href="{{url('/')}}">Our Work</a></li>
                                <li><a href="{{url('/reels')}}">Reels</a></li>
                                <li class="active"><a href="{{url('/contacts')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- End navbar -->
            </div>

            <!-- End navigation bar -->
        </div>
    </header>
    <div class="contact">
        <div class="menu-overlay" style="display: none ; background-color: white ; opacity: 1"></div>
        <div class="container">
            <div class="contact-content ">
                <div class="contact-left animated bounceIn  ">
                    <div class="left-content">
                        <div class="text-location">
                            <p class="d-flex">
                                <span class="fa fa-map-marker" style="margin-right: 10px"></span>
                                <span>66 Man Thien Street, Tang Nhon Phu A Ward <br>Quan 9 , Ho Chi Minh City</span>
                            </p>
                        </div>
                        <div class="text-phone">
                            <p class="d-flex">
                                <span class="fa fa-phone" style="margin-right: 10px"></span>
                                <span>0366-403-210</span>
                            </p>
                        </div>
                        <div class="text-phone">
                            <p class="d-flex">
                                <span class="fa fa-envelope-o" style="margin-right: 10px"></span>
                                <span>haole042@gmail.com</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="contact-right animated bounceIn ">
                    <iframe
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3244.275168490947!2d138.54770631561047!3d35.596278342232964!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601bf085981f7b9f%3A0x27e0748f88acd0a1!2sCalc!5e0!3m2!1svi!2sjp!4v1572489245395!5m2!1svi!2sjp"
                            width="100%" height="400px" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="gallery">

                <div class="gallery-item animated bounceInLeft">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1488190211105-8b0e65b80b4e?w=500&h=500&fit=crop"
                         alt="person writing in a notebook beside by an iPad, laptop, printed photos, spectacles, and a cup of coffee on a saucer">
                </div>

                <div class="gallery-item animated bounceInLeft" style="animation-delay: 0.1s;">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1515260268569-9271009adfdb?w=500&h=500&fit=crop"
                         alt="sunset behind San Francisco city skyline">
                </div>

                <div class="gallery-item animated bounceInLeft" style="animation-delay: 0.2s;">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1506045412240-22980140a405?w=500&h=500&fit=crop"
                         alt="people holding umbrellas on a busy street at night lit by street lights and illuminated signs in Tokyo, Japan">
                </div>

                <div class="gallery-item animated bounceInRight" style="animation-delay: 0.3s;">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1514041181368-bca62cceffcd?w=500&h=500&fit=crop"
                         alt="car interior from central back seat position showing driver and blurred view through windscreen of a busy road at night">
                </div>

                <div class="gallery-item animated bounceInRight" style="animation-delay: 0.4s;">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1445810694374-0a94739e4a03?w=500&h=500&fit=crop"
                         alt="back view of woman wearing a backpack and beanie waiting to cross the road on a busy street at night in New York City, USA">
                </div>

                <div class="gallery-item animated bounceInRight" style="animation-delay: 0.5s;">
                    <img class="gallery-image"
                         src="https://images.unsplash.com/photo-1486334803289-1623f249dd1e?w=500&h=500&fit=crop"
                         alt="man wearing a black jacket, white shirt, blue jeans, and brown boots, playing a white electric guitar while sitting on an amp">
                </div>

            </div>
            <div class="introduce animated bounceInDown">
                <p>At Carbon, we use design and technology to realize our client’s ideas, helping them reach their
                    target audience
                    through eye-catching and engaging visual storytelling. Taking inspiration from art, cinema, design
                    and the world
                    around us, we dissect the concept to its core, then develop it further, exploring routes and ideas
                    that might
                    otherwise be overlooked, until we reach the best possible solution for every brief.<br>
                    <br>
                    From character animation to photo-real VFX, and invisible finishing to color grading, we push beyond
                    the
                    boundaries of the budget. Drawing on our collective decades of experience, we create a streamlined
                    and efficient
                    workflow and work we are proud of, whilst keeping it fun and personal among our staff.</p>
            </div>
        </div>
        <form class="animated bounceInUp" action="{{route('contact_post')}}" method="POST" enctype="multipart/form-data"
              accept-charset="utf-8">
            {{ csrf_field() }}
            <div class="form-content view-mark in-view" data-in-view=".view-mark, .contact .column17">
                <h1>Want to work at The Hung?</h1>
                <div class="main-fields">
                    <div class="field">
                        <label for="name">Name</label>
                        <input id="name" name="name" type="text">
                    </div>
                    <div class="field">
                        <label for="name">Phone</label>
                        <input id="phone" name="phone" type="text">
                    </div>

                    <div class="field">
                        <label for="email">Email</label>
                        <input id="email" type="email" name="email">
                    </div>

                    <div class="field">
                        <label for="current-location">Where do you live now?</label>
                        <input id="current-location" type="text" name="address">
                    </div>

                    <div class="field">
                        <label>What position are you interested in?</label>
                    </div>
                    <textarea style="width: 100%;" name="message" id="position" rows="4" cols="50"> </textarea>
                </div>
                <div class="file-upload active">
                    <div class="file-select">
                        <div class="buttons">
                            <label class="custom-upload"><input type="file" id="chooseFile" name="file_contact"/>Attach
                                Resume</label>
                            <label class="custom-upload" style="margin-left: 10px"><input type="submit"/>Submit
                                Form</label>
                        </div>
                        <div class="file-show">
                            <div class="file-select-name" style="display: none" id="noFile"></div>
                        </div>
                    </div>

                </div>


            </div>
        </form>
    </div>
@endsection
