
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('/img/page/favicon.png')}}" sizes="32x32">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TheHung') }}</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!--Owl Slider -->
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.theme.default.min.css')}}">
    <!-- Font Awesome Icons -->

    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/repository.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <!-- Style css -->
    <script>(function (d, s, id) {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=2488139114537079&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146851053-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-146851053-1');
    </script>
</head>
<body class="home">
<div class="wrapper">
    <header class="header header-detail ">
        <div id="header-wrapper" class=" header-wrapper" role="main">
            <!-- Top navigation bar -->
            <div class=" row-header">
                <!-- navbar -->
                <nav>
                    <div class="row menu-header animated fadeInDown">
                        <div class="col-lg-3 col-12 icon-logo">
                            <!-- Toggle menu button -->
                            <div data-toggle="slide-collapse" data-target="#top_navbar">
                                <button type="button" class="btn navbar-toggler main-icon">
                                    <div id="nav-icon1">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </button>
                            </div>
                            <div class="logo-menu">
                                <a href="{{url('/')}}" class="logo">
                                    <img src="{{asset('img/page/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End btn office tours -->
                        <div class="col-lg-9 col-sm-12" id="top_navbar">
                            <ul class="menu">
                                <li class="active"><a href="{{url('/')}}">Our Work</a></li>
                                <li><a href="{{url('/reels')}}">Reels</a></li>
                                <li><a href="{{url('/contacts')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- End navbar -->
            </div>

            <!-- End navigation bar -->
        </div>
    </header>
    <section class="landing work-detail " style=" background-position: center center;">
        <div class="video-wrapper" style="    background-color: #45a8bf;">
            <div class="overlay"></div>
        </div>
        <div class="content-wrap">
            <div class="stage size-parent">
                <div class="video-wrap">
                    <div class="stage size-parent">
                        <div class="video-wrap">
                            <iframe width="100%" src="{{asset('img/video/'.$news->video)}}" frameborder="0"
                                    allow="accelerometer; autoplay encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>

                        </div>
                    </div>
                </div>
            </div>
            <div class="meta-wrap">
                <div class="meta">
                    <div class="title-wrap">
                        <div class="client">Project</div>
                        <div class="title">{{$news->tittle_vn}}</div>
                    </div>
                    <div class="credits-wrap">
                        <div class="director">
                            <span class="label">VFX/CGI</span>
                            <span class="name">The Hung Man Productions</span>
                        </div>
                        <div class="agency">
                            <span class="label">Position</span>
                            <span class="name">Engineer</span>
                        </div>
                    </div>
                </div>
                <div class="campaign-arrow">
                    <span></span>
                    <a href="#header-wrapper" class="scroll">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12" height="19" viewBox="0 0 11.29 18.19" class="svg"
                             src="https://carbonvfx.com/wp-content/themes/carbon-vfx-wordpress/images/arrow-down.svg">
                            <polygon
                                    points="11.29 12.55 10.59 11.84 6.15 16.28 6.15 0 5.15 0 5.15 16.28 0.71 11.84 0 12.55 5.65 18.19 11.29 12.55"
                                    fill="#fff"></polygon>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
        <div class="menu-overlay" style="display: none ; background-color: white ; opacity: 1"></div>
    </section>
    <section class="list-content animated bounceInDown">
        <div class="container ">
            <div class="detail-content">
                {!! $news['content_vn'] !!}
                <button class="button main">Credits</button>
            </div>

        </div>
        <div class="credits-section">
            <dive class=" close_credit">
                <i class="fa fa-close"></i>
            </dive>

            <div class="credits-content container ">
                <div class="inner-wrap ">
                    <div>
                        <div class="credits-block width-1">
                            <h3 class="label">Carbon</h3>
                            <ul class="credits-list">

                                <li><span class="line line-1">Colorist</span> <span class="line line-2">Julien Biard</span></li>

                                <li><span class="line line-1">Color Assistant</span> <span class="line line-2">Hatice Decker</span></li>


                                <li><span class="line line-1">Executive Producer</span> <span class="line line-2">Gretchen Praeger</span>
                                </li>


                                <li><span class="line line-1">Producer</span> <span class="line line-2">Lauryn Grimando</span></li>


                                <li><span class="line line-1">Producer</span> <span class="line line-2">Laurie Andrianopoli</span></li>


                                <li><span class="line line-1">On-Set VFX Sup</span> <span
                                            class="line line-2">Aleksander Sasha Djordjevic</span></li>


                                <li><span class="line line-1">Flame Supervisor</span> <span class="line line-2">Michael Sarabia</span>
                                </li>


                                <li><span class="line line-1">Flame Artist</span> <span class="line line-2">Heidi Anderson</span></li>


                                <li><span class="line line-1">Flame Artist</span> <span class="line line-2">Jaimie Beckwith</span></li>


                                <li><span class="line line-1">CG Supervisor</span> <span class="line line-2">Tim Little</span></li>


                                <li><span class="line line-1">Nuke Artist</span> <span class="line line-2">Adam Thompson</span></li>


                                <li><span class="line line-1">Nuke Artist</span> <span class="line line-2">Daniel Noren</span></li>


                                <li><span class="line line-1">Nuke Artist</span> <span class="line line-2">Jen Howard</span></li>


                                <li><span class="line line-1">Nuke Artist</span> <span class="line line-2">Matt Doll</span></li>


                            </ul>
                        </div>
                    </div>
                    <div>
                        <div class="credits-block width-1">
                            <h3 class="label">External</h3>
                            <ul class="credits-list">


                                <li><span class="line line-1">Client</span> <span class="line line-2">Porsche Cars North America</span>
                                </li>


                                <li><span class="line line-1">Agency</span> <span class="line line-2">Cramer-Krasselt</span></li>


                                <li><span class="line line-1">Production Company</span> <span
                                            class="line line-2">Hungry Man Productions</span></li>


                                <li><span class="line line-1">Director</span> <span class="line line-2">Wayne McClammy</span></li>


                                <li><span class="line line-1">Producer</span> <span class="line line-2">Rick Jarjoura</span></li>


                                <li><span class="line line-1">Editorial</span> <span class="line line-2">Whitehouse Post</span></li>


                                <li><span class="line line-1">Editor</span> <span class="line line-2">Adam Marshall</span></li>


                                <li><span class="line line-1">Assistant Editor</span> <span class="line line-2">Steven Kroodsma</span>
                                </li>


                                <li><span class="line line-1">Audio</span> <span class="line line-2">Another Country</span></li>


                            </ul>
                        </div>

                    </div>
                    <p>
                    </p></div>
            </div>

        </div>
        <footer class="footer ">
    <span data-wow-delay="0.2s" class="span3 wow rollIn"
          style="visibility: visible; animation-delay: 0.2s; animation-name: rollIn;">
      <i class="fa fa-facebook-square"></i>
    </span>
            <span data-wow-delay="0.6s" class="span3 wow rollIn"
                  style="visibility: visible; animation-delay: 0.6s; animation-name: rollIn;">
      <i class="fa fa-google"></i>
    </span>
            <span data-wow-delay="1s" class="span3 wow rollIn"
                  style="visibility: visible; animation-delay: 1s; animation-name: rollIn;">
      <i class="fa fa-instagram"></i>
    </span>

            <!--Linkedin-->
        </footer>
    </section>
</div>

<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{asset('lib/OwlCarousel2/owl.carousel.min.js')}}"></script>
<script src="{{ asset('js/detail.js') }}"></script>

@stack('page-scripts')
</body>
</html>
