@extends('admin.app')

@section('title', 'SKR | User')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item ">{{__('admin/layout.company_c_list')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{__('admin/layout.success')}}</strong> {{session('status')}}
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">Active Update Company Contact</div>
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <form action="" enctype="multipart/form-data" accept-charset="utf-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('/admin/company-contact-add')}}">
                                                <button type="button" class="btn  btn-primary float-right">
                                                    <i class="fa fa-plus"></i>
                                                    {{__('admin/layout.created_btn')}}</button>
                                            </a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="company_ct_list" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.code')}}</th>
                                            <th>{{__('admin/layout.phone')}}</th>
                                            <th>{{__('admin/layout.email')}}</th>
                                            <th>{{__('admin/layout.fax')}}</th>
                                            <th>{{__('admin/layout.company_address')}}</th>
                                            <th class="btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteUser" tabindex="-1" role="dialog" aria-labelledby="modelLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            {{__('admin/layout.confirm_d')}}
                        </div>
                        <div id="confirmMessage" class="modal-body">
                            {{__('admin/layout.delete_c')}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteCompany($(this).val())">
                                {{__('admin/layout.delete')}}
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal"  data-toggle="modal" data-target="#confirmDeleteUser">
                                {{__('admin/layout.cancel')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        function checkContactExisted(id) {
            $('#btnConfirmDelete').val(id);
        }
        function deleteCompany(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('company_ct_delete')}}",
                method: "post",
                data: {
                    Id: id
                }
            }).done(function(data) {
                if (data == 0) {
                    location.reload(true);
                } else {
                    $("#confirmDeleteNew").modal('hide');
                }
            })

        }
        let url=" ";
        let edit_btn ="{{__('admin/layout.edit')}}";
        let delete_btn="{{__('admin/layout.delete')}}";
        @if(session('lang')=='jp')
            url ="{{asset('lib/datatables/table.json')}}";
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $("#company_ct_list").DataTable({
            "language": {
                url: url
            },
            "processing": true,
            "lengthChange": true,
            "pageLength": 10,
            "serverSide": true,
            "ajax": {
                "url": "{{route('api.company_ct.index')}}",
                "type": 'GET',
            },

            "columns": [
                {
                    "data": "code_company"
                },
                {
                    "data": "phone"
                },
                {
                    "data": "mail"

                },
                {
                    "data": "fax"
                },
                {
                    "data": "address"
                },
                {
                    "data": null,
                    "render": function (data, type, row, meta) {
                        edit_href = "{{url('admin/company-contact-edit')}}/" + data.id;
                        delete_href = ' onclick = checkContactExisted(' + data.id + ') ';
                        return '<button class="btn btn-success btn-xs" ><a href=' + edit_href + '><span class="fa fa-edit"></span>'+edit_btn+'</a></button>' +
                            '<button class="btn btn-xs btn-danger delete-user" ' + delete_href + 'data-toggle="modal"' +
                            ' data-target="#confirmDeleteUser"> <span class="fa fa-trash" > </span> '+delete_btn+'</a></button>'
                    }
                },

            ],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [[0, 'asc']], rowCallback: function (row, data) {
                // Set the checked state of the checkbox in the table
                $('input.editor-active', row).prop('checked', data.active == 1);
            }
        });

    </script>

@endpush

