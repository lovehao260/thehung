@extends('admin.app')
@section('title', 'SKR | Company')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/admin/company-contact')}}" class="text-black">{{__('admin/layout.company_c_list')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.add_company_c')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <form action="{{route('post_company_ct_add')}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label class="mdb-main-label">{{__('admin/layout.company_code')}} </label>
                                            <select class="custom-select" required name="code_company">
                                                <option value="">{{__('admin/layout.company_select')}}</option>
                                                @foreach($code as $row)
                                                    <option value="{{$row->code}}">{{$row->code}} </option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('code_company')) <span
                                                    class="text-danger">{{$errors->first('code_company')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="company_phone">{{__('admin/layout.phone')}}</label>
                                            <input id="company_phone" name="phone" type="text" required
                                                   value="{{old('phone')}}" class="form-control
                                                    @if($errors->has('phone')) border border-danger @endif"
                                                   placeholder="Phone">
                                            @if ($errors->has('phone')) <span
                                                    class="text-danger">{{$errors->first('phone')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="company_mail">{{__('admin/layout.email')}}</label>
                                            <input id="company_mail" name="mail" type="email" required
                                                   value="{{old('mail')}}" class="form-control
                                                    @if($errors->has('mail')) border border-danger @endif"
                                                   placeholder="Mail Company">
                                            @if ($errors->has('mail')) <span
                                                    class="text-danger">{{$errors->first('mail')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="company_fax">{{__('admin/layout.fax')}}</label>
                                            <input id="company_fax" name="fax" type="text"
                                                   value="{{old('fax')}}" class="form-control
                                                    @if($errors->has('fax')) border border-danger @endif"
                                                   placeholder="Fax Company">
                                            @if ($errors->has('fax')) <span
                                                    class="text-danger">{{$errors->first('fax')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="company_sdt">{{__('admin/layout.hotline')}}</label>
                                            <input id="company_sdt" name="hotline" type="text"
                                                   value="{{old('hotline')}}" class="form-control
                                                    @if($errors->has('hotline')) border border-danger @endif"
                                                   placeholder="Hot Line Company">
                                            @if ($errors->has('fax')) <span
                                                    class="text-danger">{{$errors->first('hotline')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="address_cm">{{__('admin/layout.company_address')}}</label>
                                            <textarea name="address" rows="10" required
                                                      class="form-control  @if($errors->has('address'))
                                                              dborder border-danger @endif">{{old('address')}}</textarea>
                                            @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert" style="color: red">
                                        <strong>{{ $errors->first('address') }}</strong>
                                      </span>
                                            @endif
                                            @if ($errors->has('address')) <span
                                                    class="text-danger">{{$errors->first('address')}}</span> @endif
                                        </div>
                                       <!--/Blue select-->
                                        <div class="d-flex justify-content-between bd-highlight mb-3">
                                            <div class="p-2 bd-highlight">
                                                <a href="{{url('admin/company-contact')}}">
                                                    <Button type="button" class="btn  btn-primary btn-outline-danger">
                                                        {{__('admin/layout.cancel')}}
                                                    </Button>
                                                </a>
                                            </div>
                                            <div class="p-2 bd-highlight">
                                                <Button type="submit" class="btn  btn-primary ">{{__('admin/layout.created_btn')}}</Button>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
@endsection
