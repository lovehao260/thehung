@extends('admin.app')

@section('title', 'SKR | User')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.user_list')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong> {{session('status')}}</strong>
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">Now Update your Search
                        </div>
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <form action="" enctype="multipart/form-data" accept-charset="utf-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('/admin/user-add')}}">
                                                <button type="button"
                                                        class="btn  btn-primary float-right">
                                                    <i class="fa fa-plus"></i>
                                                    {{__('admin/layout.created_btn')}}</button>
                                            </a>
                                        </div>
                                    </div>

                                </form>
                            </div>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="user_list" class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.name')}}</th>
                                            <th>{{__('admin/layout.username')}}</th>
                                            <th>{{__('admin/layout.email')}}</th>
                                            <th>{{__('admin/layout.auto_mail')}}</th>
                                            <th style="width: 100px">{{__('admin/layout.password')}}</th>
                                            <th class="btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteUser" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            {{__('admin/layout.confirm_d')}}
                        </div>
                        <div id="confirmMessage" class="modal-body">
                            {{__('admin/layout.delete_c')}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                                    onclick="deleteUser($(this).val())">
                                {{__('admin/layout.delete')}}
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal"
                                    data-toggle="modal" data-target="#confirmDeleteUser">
                                {{__('admin/layout.cancel')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- The Modal -->
            <div class="modal" id="cannotDelete">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Cannot Remove User</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            Because You do not have this role
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>


@endsection
@push('page-scripts')
    <script>
        function checkContactExisted(id) {
            $('#btnConfirmDelete').val(id);
        }
        function deleteUser(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('user_delete')}}",
                method: "post",
                data: {
                    Id: id
                }
            }).done(function (data) {
                console.log(data)
                if (data == 0) {
                    location.reload(true);
                } else {
                    $("#confirmDeleteUser").modal('hide');
                    $('#cannotDelete').modal('show');
                }
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            })
        }
        function roleChanged(obj, value) {
            let role = value.value;
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('user_role')}}",
                method: "post",
                data: {
                    id: obj,
                    role: role,
                }
            }).done(function (data) {
                    if (data === "1") {
                        $("#snoAlertBox").fadeIn();
                        closeSnoAlertBox();

                    } else {
                        alert('Failure Role User ')
                    }
                }
            )
        }
        let url = " ";
        let edit_btn = "{{__('admin/layout.edit')}}";
        let delete_btn = "{{__('admin/layout.delete')}}";
        let reset_btn = "{{__('admin/layout.reset')}}";
        @if(session('lang')=='jp')
            url = "{{asset('lib/datatables/table.json')}}";
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $("#user_list").DataTable({
            "language": {
                url: url
            },
            "processing": true,
            "lengthChange": true,
            "pageLength": 10,
            "serverSide": true,

            "ajax": {
                "url": "{{route('api.user.index')}}",
                "type": 'GET',
            },
            "columns": [
                {
                    "data": "name",
                },
                {
                    "data": "username"
                },
                {
                    "data": "email"

                },
                {
                    "data": null,
                    "render": function (data, type, row) {
                        let roleCheck = '{{ Auth::user()->role }}';
                        let check = roleCheck != 0 ? "disabled" : "";
                        let none = data.role == 0 ? "selected" : "";
                        let mail = data.role == 2 ? "selected" : "";
                        return '<select class="form-control" id="selectRole" onchange = roleChanged(' + data.id + ',this)   > ' +
                            '<option value="0" ' + none + '  >None</option>' +
                            '<option value="2" ' + mail + ' >Mail</option>' +
                            '</select>'
                    }
                },
                {
                    "data": null,
                    "render": function (data, type, row, meta) {
                        edit_href = "{{url('admin/user-edit-password')}}/" + data.id;
                        return '<button style="display: block;margin: auto" class="btn btn-default btn-xs" ><a href=' + edit_href + '>' +
                            '<span class="fa fa-lock"></span> '+reset_btn+'</a></button>'
                    }
                },
            ],
            "columnDefs": [{
                "targets": 5,
                "data": null,
                "render": function (data, type, row, meta) {
                    edit_href = "{{url('admin/user-edit')}}/" + data.id;
                    delete_href = ' onclick = checkContactExisted(' + data.id + ') ';
                    return '<button class="btn btn-success btn-xs" ><a href=' + edit_href + '><span class="fa fa-edit"></span>'+edit_btn+'</a></button>' +
                        '<button class="btn btn-xs btn-danger delete-user" ' + delete_href + 'data-toggle="modal" data-target="#confirmDeleteUser">' +
                        ' <span class="fa fa-trash" > </span> '+delete_btn+'</a></button>'
                }
            }],

        });
    </script>

@endpush

