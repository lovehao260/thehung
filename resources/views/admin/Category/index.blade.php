@extends('admin.app')

@section('title', 'SKR | Category')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item ">{{__('admin/layout.categories')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{__('admin/layout.success')}}</strong> {{session('status')}}
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">Update Company Contact
                        </div>
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <form action="" enctype="multipart/form-data" accept-charset="utf-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('/admin/categories-add')}}">
                                                <button type="button" class="btn  btn-primary float-right">
                                                    <i class="fa fa-plus"></i>
                                                    {{__('admin/layout.created_btn')}}</button>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="category_list" class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 40%">{{__('admin/layout.name')}}</th>
                                            <th>{{__('admin/layout.parent')}}</th>
                                            <th>{{__('admin/layout.created')}}</th>
                                            <th>{{__('admin/layout.update')}}</th>
                                            <th class="column-key-status">{{__('admin/layout.status')}}</th>
                                            <th class="icon-lang"><img src="{{asset('/img/icon/english.jpg')}}"
                                                                       title="English" alt="English">
                                            </th>
                                            <th class="icon-lang"><img src="{{asset('img/icon/Japan-Flag-icon.png')}}"
                                                                       title="japan" alt="japan">
                                            </th>
                                            <th class="icon-lang"><img src="{{asset('/img/icon/vn.png')}}"
                                                                       title="Vietnam" alt="Vietnam">
                                            </th>
                                            <th class="btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->

                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteCompany" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            {{__('admin/layout.confirm_d')}}
                        </div>
                        <div id="confirmMessage" class="modal-body">
                            {{__('admin/layout.delete_c')}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                                    onclick="deleteCategories($(this).val())">
                                {{__('admin/layout.delete')}}
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal"
                                    data-toggle="modal" data-target="#confirmDeleteCompany">
                                {{__('admin/layout.cancel')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- The Modal -->
            <div class="modal" id="cannotDelete">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('admin/layout.company_delete_n')}}</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            {{__('admin/layout.company_delete_b')}}
                        </div>
                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger"
                                    data-dismiss="modal">{{__('admin/layout.close')}}</button>
                        </div>

                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>


@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        function checkCatExisted(id) {
            $('#btnConfirmDelete').val(id);
        }
        function deleteCategories(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('categories_delete')}}",
                method: "post",
                data: {
                    Id: id
                }
            }).done(function (data) {
                console.log(data)
                if (data == 0) {
                    location.reload(true);
                } else {
                    $('#confirmDeleteCompany').modal('hide');
                    $('#cannotDelete').modal('show');
                }
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
            })
        }
        let url = " ";
        let edit_btn = "{{__('admin/layout.edit')}}";
        let delete_btn = "{{__('admin/layout.delete')}}";
        @if(session('lang')=='jp')
            url = "{{asset('lib/datatables/table.json')}}";
        @endif
            $.fn.dataTable.ext.errMode = 'throw';
        $("#category_list").DataTable({
            "language": {
                url: url
            },
            "processing": true,
            "lengthChange": true,
            "pageLength": 10,
            "serverSide": true,
            "ajax": {
                "url": "{{route('api.categories.index')}}",
                "type": 'GET',
            },

            "columns": [
                {
                    "data": "name"
                },
                {
                    "data": "parent_id",
                },

                {
                    "data": "created_at"
                },
                {
                    "data": "updated_at"
                },
                {
                    "data": "status",
                    "render": function (data) {
                        if (data == "Published") {
                            return '<span class="label-success status-label">' + data + '</span>';
                        } else {
                            return '<span class="label-danger status-label">' + data + '</span>';
                        }
                    }
                },
                {
                    "data": null,
                    "bSortable": false,
                    "render": function (data) {
                        if (data.name_en === null) {
                            return '<i class="fa fa-close text-danger"></i>';
                        } else {
                            return '<i class="fa fa-check text-success"></i>';
                        }

                    }
                },
                {
                    "data": null,
                    "bSortable": false,
                    "render": function (data) {
                        if (data.name_jp === null) {
                            return '<i class="fa fa-close text-danger"></i>';
                        } else {
                            return '<i class="fa fa-check text-success"></i>';
                        }

                    }
                },
                {
                    "data": null,
                    "bSortable": false,
                    "render": function (data) {
                        if (data.name_vn === null) {
                            return '<i class="fa fa-close text-danger"></i>';
                        } else {
                            return '<i class="fa fa-check text-success"></i>';
                        }

                    }
                },
                {
                    "data": null,
                    "bSortable": false,
                    "render": function (data, type, row, meta) {
                        edit_href = "{{url('admin/categories-edit')}}/" + data.id;
                        delete_href = ' onclick = checkCatExisted(' + data.id + ') ';
                        return '<button class="btn btn-success btn-xs" ><a href=' + edit_href + '><span class="fa fa-edit"></span>' + edit_btn + '</a></button>' +
                            '<button class="btn btn-xs btn-danger delete-user" ' + delete_href + 'data-toggle="modal" data-target="#confirmDeleteCompany">' +
                            ' <span class="fa fa-trash" > </span> ' + delete_btn + '</a></button>'
                    }
                },
            ],
            "order": [],

        });

    </script>

@endpush

