@extends('admin.app')
@section('title', 'SKR | Category')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item  "><a href="{{url('/admin/categories')}}" class=" text-black">{{__('admin/layout.categories')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.edit_cat')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class=" card-outline">
                        <!-- /.card-header -->
                        <div class="card-body category">
                            <form action="{{route('post_category_edit',$data->id)}}" method="POST" enctype="multipart/form-data"
                                  accept-charset="utf-8">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-7 card card-primary p-2">
                                        <div class="category-">
                                            <label for="category_url">{{__('admin/layout.permalink')}}</label>
                                            <div class="d-flex  text-black">
                                                <div class="">
                                                      <span class="default-slug">{{url('/')}}/
                                                          <span id="editable-post-name"></span>
                                                      </span>
                                                </div>
                                                <div class="w-25 input-slug">
                                                    <input id="category_url" name="slug" type="text" required
                                                           value="{{old('slug',$data->slug)}}" class="form-control
                                                    @if($errors->has('slug')) border border-danger @endif"
                                                           placeholder="Url Categories">
                                                </div>
                                            </div>
                                            @if ($errors->has('slug')) <span
                                                    class="text-danger">{{$errors->first('slug')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="category_name">{{__('admin/layout.name')}}</label>
                                            <input id="category_name" name="name" type="text" required
                                                   value="{{old('name',$data->name_en)}}" class="form-control
                                                    @if($errors->has('name')) border border-danger @endif"
                                                   placeholder="Name">
                                            @if ($errors->has('name')) <span
                                                    class="text-danger">{{$errors->first('name')}}</span> @endif
                                        </div>
                                        <div class="form-group  @if($data->name_vn===null)vietnam-input @endif">
                                            <label for="name-vn">{{__('admin/layout.name_vn')}}</label>
                                            <input id="name-vn" name="name_vn" type="text"
                                                   value="{{old('name_vn',$data->name_vn)}}" class="form-control
                                                    @if($errors->has('name_vn')) border border-danger @endif"
                                                   placeholder="Name Vietnam">
                                            @if ($errors->has('name_vn')) <span
                                                    class="text-danger">{{$errors->first('name_vn')}}</span> @endif
                                        </div>
                                        <div class="form-group  @if($data->name_jp===null)japan-input @endif">
                                            <label for="name_jp">{{__('admin/layout.name_jp')}}</label>
                                            <input id="category_name" name="name_jp" type="text"
                                                   value="{{old('name_jp',$data->name_jp)}}" class="form-control
                                                    @if($errors->has('name_jp')) border border-danger @endif"
                                                   placeholder="Name Japan">
                                            @if ($errors->has('name_jp')) <span
                                                    class="text-danger">{{$errors->first('name_jp')}}</span> @endif
                                        </div>

                                        <div class="form-group ">
                                            <label for="company_name">{{__('admin/layout.parent')}}</label>
                                            <select class="form-control" name="parent_id">
                                                <option value="0">None</option>
                                                @foreach($categories as $cat)
                                                    <option value="{{$cat->id}}" @if ($data->parent_id == $cat->id) selected
                                                            @endif>{{$cat->name_en}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="description"> {{__('admin/layout.description')}}</label>
                                            <textarea name="description" rows="4" placeholder="Short description"
                                                      class="form-control  @if($errors->has('description'))
                                                              dborder border-danger @endif">{{old('description',$data->description)}}</textarea>
                                            @if ($errors->has('description')) <span
                                                    class="text-danger">{{$errors->first('description')}}</span> @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="Sort">{{__('admin/layout.sort')}}</label>
                                            <input id="Sort" name="sort" type="number" min="1" max="99" required
                                                   value="{{old('sort',$data->sort)}}" class="form-control
                                                    @if($errors->has('sort')) border border-danger @endif"
                                                   placeholder="Sort Categories">
                                            @if ($errors->has('sort')) <span
                                                    class="text-danger">{{$errors->first('sort')}}</span> @endif
                                        </div>

                                    </div>
                                    <div class="col-md-5 right-categories">
                                        <div class="card card-primary p-2">
                                            <div class="widget-title">
                                                <h4>
                                                    <span>{{__('admin/layout.publish')}}</span>
                                                </h4>
                                            </div>
                                            <div class="d-flex bd-highlight mb-3">

                                                <div class="p-2 bd-highlight">

                                                    <Button type="submit" class="btn  btn-primary ">
                                                        <i class="fa fa-save"></i>
                                                        {{__('admin/layout.save')}}
                                                    </Button>
                                                </div>
                                                <div class="p-2 bd-highlight">
                                                    <a href="{{url('admin/categories')}}">
                                                        <Button type="button"
                                                                class="btn  btn-primary btn-outline-danger">
                                                            {{__('admin/layout.cancel')}}
                                                        </Button>
                                                    </a>
                                                </div>

                                            </div>

                                            <div><strong>{{__('admin/layout.translations')}}</strong>
                                                <div id="list-others-language" onclick="inputVN()">
                                                    <img src="https://cms.botble.com/vendor/core/images/flags/vn.png"
                                                         title="Tiếng Việt" alt="Tiếng Việt">
                                                    {{trans('admin/layout.vietnam')}}  <i class="fa fa-plus"></i>
                                                    <br>
                                                </div>
                                                <div id="list-others-language" onclick="inputJp()">
                                                    <img src="{{asset('img/icon/Japan-Flag-icon.png')}}" style="max-width: 16px"
                                                         title="{{trans('admin/layout.japan')}}" alt="Tiếng Việt">
                                                    {{trans('admin/layout.japan')}} <i class="fa fa-plus"></i>
                                                    <br>
                                                </div>
                                            </div>
                                            <div class="widget-title">
                                                <h4>
                                                    <span>{{__('admin/layout.status')}}</span>
                                                </h4>
                                            </div>
                                            <div class="form-group ">
                                                <select class="form-control" name="status">
                                                    <option value="Published" @if ($data->status == "Published") selected
                                                            @endif>Published</option>
                                                    <option value="Draft"  @if ($data->status == "Draft") selected
                                                            @endif>Draft</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <script>

        function inputVN() {
            $(".vietnam-input").show();
        }
        function inputJp() {
            $(".japan-input").show();
        }
    </script>
@endsection

