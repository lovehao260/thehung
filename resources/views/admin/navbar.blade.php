
<section class="main-header ">
    <a href="{{url('admin')}}" class="logo" >
        <span class="logo-mini"><b>P</b>S</span>
        <img src="{{asset('img/page/logo.png')}}" alt="logo" class="logo-default" style="max-width: 120px;margin-right: 20px">
    </a>
    <nav class="main-header navbar navbar-expand border-bottom navbar-dark">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="{{url('/admin')}}" class="nav-link">{{__('admin/layout.home')}}</a>
            </li>
        </ul>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class=" dropdown-header-name text-white" style="padding-right: 10px;text-decoration: none;" href="{{url('/')}}"
                   target="_blank"><i class="fa fa-globe"></i> <span class="d-none d-sm-inline">{{__('admin/layout.web_index')}} </span> </a>
            </li>
            <li class="nav-item">
            <li class="dropdown dropdown-extended dropdown-inbox " id="header_inbox_bar">
                <a class="dropdown-toggle dropdown-header-name text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-envelope"></i>
                    <span class="badge badge-default"> {{$contacts_view}} </span>
                </a>
                @if($contacts_view > 0)
                <ul class="dropdown-menu dropdown-menu-right ">
                    <li class="external">
                        <h3>{{__('admin/layout.message_noti')}}</h3><br>
                     </li>
                    <li>
                        <ul class="dropdown-menu-list scroller" data-handle-color="#637283">
                            @foreach($contacts as $row)
                            <li>
                                <a href="{{url('admin/contact/detail/'.$row->id)}}">
                                    <div class="d-flex photo">
                                        <span class="p-2 text-blue ">    <img src="https://cdnjs.cloudflare.com/ajax/libs/admin-lte/2.4.18/img/avatar5.png" class="rounded-circle" alt="{{$row->name}}"></span>
                                        <div class="p-2 text-blue"><span class="from"> {{$row->name}}</span><br>
                                            <span class="time">{{$row->created_at}} </span>  </div>
                                    </div>
                                </a>
                            </li>
                                @endforeach
                        </ul>
                    </li>
                    <li class="footer text-center">
                          <a href="{{url('/admin/contact')}}">{{__('admin/layout.view_all')}}</a>
                    </li>
                </ul>
                 @endif
            </li>

        </ul>

    </nav>
</section>
<script>

</script>
