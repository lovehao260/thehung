@extends('admin.app')
@section('title', 'SKR | New')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.news_list')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible" id="status">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>{{__('admin/layout.success')}}</strong> {{session('status')}}
                            </div>
                        @endif
                        <div id="snoAlertBox" class="alert alert-success" data-alert="alert">success News Status
                            update
                        </div>
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <form action="" enctype="multipart/form-data" accept-charset="utf-8">
                                    <div class="row">
                                        <div class="col-md-6">
                                        </div>
                                        <div class="col-md-6">
                                            <a href="{{url('admin/new-add')}}">
                                                <button type="button" class="btn  btn-primary float-right">
                                                    <i class="fa fa-plus"></i>
                                                    {{__('admin/layout.created_btn')}}</button>
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- /.card-body -->
                            <div class="card-body ">
                                <div class="table-responsive">
                                    <table id="news_list" class="table table-bordered table-hover ">
                                        <thead>
                                        <tr>
                                            <th>{{__('admin/layout.url')}}</th>
                                            <th>{{__('admin/layout.avatar')}}</th>
                                            <th style="width: 150px">{{__('admin/layout.author')}}</th>
                                            <th style="width: 80px">{{__('admin/layout.status')}}</th>
                                            <th style="width: 150px">{{__('admin/layout.created')}}</th>
                                            <th class="text-center btn-action">{{__('admin/layout.action')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->
            <!-- confirm dialog-->
            <div class="modal fade" id="confirmDeleteNew" tabindex="-1" role="dialog" aria-labelledby="modelLabel"
                 aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            {{__('admin/layout.confirm_d')}}
                        </div>
                        <div id="confirmMessage" class="modal-body">
                                                    {{__('admin/layout.delete_n')}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok"
                                    onclick="deleteNew($(this).val())">
                                {{__('admin/layout.delete')}}
                            </button>
                            <button type="button" id="confirmCancel" class="btn btn-cancel" data-dismiss="modal">
                                {{__('admin/layout.cancel')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>

@endsection
@push('page-scripts')
    <!-- DataTables -->
    <script>
        let url=" ";
        let edit_btn ="{{__('admin/layout.edit')}}";
        let delete_btn="{{__('admin/layout.delete')}}";
        @if(session('lang')=='jp')
           url ="{{asset('lib/datatables/table.json')}}";
        @endif
        $.fn.dataTable.ext.errMode = 'throw';
        $("#news_list").DataTable({
            "language": {
                url: url
            },
            "processing": true,
            "pageLength": 10,
            "ajax": {
                "url": "{{route('api.new.index')}}",
                "type": 'GET',
            },
            "columns": [

                {
                    "data": "slug"
                },
                {
                    "data": "avatar",
                    "render": function (data, type, row, meta) {
                        img = "{{asset('img/upload')}}/" + data;
                        return '<img src="' + img + '" style="max-width:50px ">';
                    }
                },
                {
                    "data": "author"
                },
                {
                    "data": null,
                    "render": function (data, type, row) {
                        if (data.status === 0) {
                            return '<select class="form-control" id="selectSatus" onchange = statusChanged(' + data.id + ',this)  > ' +
                                '<option value="0" >New</option>' +
                                '<option value="1" >Hot</option>' +
                                '</select>'
                        } else {
                            return '<select class="form-control" id="selectSatus" onchange = statusChanged(' + data.id + ',this)  > ' +
                                '<option value"1" >Hot</option>' +
                                '<option value="0" >New</option>' +
                                '</select>'
                        }
                    }
                },
                {
                    "data": "created_at",
                    "render": function (data) {
                        let date = new Date(data);
                        let month = date.getMonth() + 1;
                        return date.getFullYear() + "-" + (date.getDate() < 10 ? "0" + date.getDate() : " " + date.getDate()) + "-" + (month < 10 ? "0" + month : " " + month);
                    }
                },
            ],

            "columnDefs": [{
                "targets": 5,
                "data": null,
                "render": function (data, type, row, meta) {
                    edit_href = "{{url('admin/new-edit')}}/" + data.id;
                    delete_href = ' onclick = checkNewExisted(' + data.id + ') ';
                    return '<button class="btn btn-success btn-xs" ><a href=' + edit_href + '><span class="fa fa-edit"></span>'+edit_btn+'</a></button>' +
                        '<button class="btn btn-xs btn-danger delete-new"  ' + delete_href + ' data-toggle="modal" data-target="#confirmDeleteNew" >' +
                        '<span class="fa fa-trash" ></span>'+delete_btn+' </a></button>'
                }
            }],
            "order": [[3, "desc"]],


        });

        function checkNewExisted(id) {
            $('#btnConfirmDelete').val(id);
        }

        function deleteNew(id) {
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('new_delete')}}",
                method: "post",

                data: {
                    Id: id
                }
            }).done(function (data) {
                if (data == 0) {
                    location.reload(true);
                } else {
                    $("#confirmDeleteContact").modal('hide');
                }
            })
        }

        function statusChanged(id, select) {
            let status = select.value;
            request = $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{route('news_hot')}}",
                method: "post",
                data: {
                    id: id,
                    status: status,
                }
            }).done(function (data) {
                    if (data === "1") {
                        $("#snoAlertBox").fadeIn();
                        closeSnoAlertBox();

                    } else {
                        alert('Failure Role User ')
                    }
                }
            )
        }





    </script>

@endpush
