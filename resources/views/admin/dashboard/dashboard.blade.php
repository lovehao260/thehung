@extends('admin.app')

@section('title', 'SKR | Dashboard')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
                            <li class="breadcrumb-item ">{{__('messages.dashboard')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-lg-4 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3>{{$count_news}}</h3>
                                <p>News</p>
                            </div>
                            <div class="icon">
                                <i class="ion-ios-calendar-outline"></i>
                            </div>
                            <a href="{{url('/admin/news')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3>{{$count_users}}</h3>
                                <p>Users</p>
                            </div>
                            <div class="icons-admin">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="{{url('/admin/user')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-xs-12">
                        <!-- small box -->
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3>{{$count_contacts}}</h3>
                                <p>Contacts</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-person-add"></i>
                            </div>
                            <a href="{{url('/admin/contact')}}"class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <!-- ./col -->
                    <!-- ./col -->
                </div>
                <div>
                    <!-- BAR CHART -->
                    <div class="row">
                        <div class="col-lg-6 col-xs-12">
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <h3 class="box-title">News Chart</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body" style="">
                                    <canvas id="myChart" width="200" height="140"></canvas>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                        <div class="col-lg-6 col-xs-12">
                            <div class="box box-danger">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Contact Chart</h3>
                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                        </button>
                                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <div class="box-body" style="">
                                    <canvas id="newChart" width="200" height="140"></canvas>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->

    </div>
    <!-- CHART JS -->
    <script src="{{asset('lib/chart.js/Chart.min.js')}}"></script>
    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var newChar = document.getElementById('newChart').getContext('2d');
        $.ajax({
            method: 'GET',
            url: '{{route('api.chart.new')}}',
            dataType: "json",
            success: function(res) {
                let views=[];
                let d = new Date();
                let y = d.getFullYear();
                $.each( res, function( key, value ) {
                    views.push(value)
                });
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels:  ["Jan","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC "] ,
                        datasets: [
                            {
                                label:'News of '+ y,
                                data: views,
                                borderColor: 'rgba(0, 128, 128, 0.7)',
                                borderWidth: 1
                            }
                        ]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        },
                    }
                });
            },
            error: function(err) {
                console.log(err);
                window.alert('Error Chart');
            }
        });

        var contact = new Chart(newChar, {
            type: 'pie',
            data: {
                datasets: [{
                    data: [{{$contacts_dont}}, {{$contacts_seen}}, {{$contacts_mail}}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.6)',
                        'rgba(54, 162, 235, 0.6)',
                        'rgba(255, 206, 86, 0.6)',
                    ],
                }],
                labels: ['Not Seen', 'Seen', 'Relay Mail']
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
            }
        });

    </script>

@endsection
