@extends('admin.app')

@section('title', 'SKR | Dashboard')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header" style="border-bottom: #0c5460 1px solid">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-left">
                            <li class="breadcrumb-item"><i class="fa fa-home "></i><a href="{{url('/admin')}}" class="text-black">{{__('admin/layout.home')}}</a></li>
                            <li class="breadcrumb-item active">{{__('admin/layout.view_contact')}}</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary card-outline">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label for="tittle_new">{{__('admin/layout.name')}}</label>
                                        <input id="tittle_new" name="name" type="text"
                                               value="{{$details->name}}" class="form-control"
                                               disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="tittle_new">{{__('admin/layout.email')}}</label>
                                        <input id="tittle_new" name="new_tittles" type="text"
                                               value="{{$details->email}}" class="form-control" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="exchange_rate">{{__('admin/layout.message')}}</label>
                                        <textarea name="new_contents" rows="10" class="form-control "
                                                  disabled>{{$details->message}}</textarea>
                                    </div>
                                    <form action="{{route('download_file')}}" method="POST" enctype="multipart/form-data"
                                          accept-charset="utf-8">
                                        {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="file_contact">File</label>
                                        <input id="file_contact" name="file_contact" type="text"
                                               value="{{$details->file_contact}}" class="form-control" readonly>
                                        <button type="submit" class="btn btn-large pull-right btn-danger m-2"><i class="icon-download-alt"> </i> Download Now </button>
                                    </div>
                                    </form>
                                    <div class="d-flex justify-content-between bd-highlight mb-3">
                                        <div class="p-2 bd-highlight">
                                            <a href="{{url('admin/contact')}}">
                                                <Button type="button" class="btn  btn-primary btn-outline-danger">
                                                    {{__('admin/layout.cancel')}}
                                                </Button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form action="{{route('relay_mail')}}" method="post" enctype="multipart/form-data"
                      accept-charset="utf-8">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Mail Reply</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <input value="{{$details->id}}" style="display: none" name="id">
                            <label for="recipient-name" class="col-form-label">Customer:</label>
                            <input type="text" class="form-control" id="recipient-name" name="name"
                                   value="{{$details->name}}" readonly>
                            <label for="recipient-name" class="col-form-label">Mail to:</label>
                            <input type="text" class="form-control" id="recipient-name" name="email"
                                   value="{{$details->email}}" readonly>
                            <input style="display: none" type="text" class="form-control" id="recipient-contact"
                                   name="message"
                                   value="{{$details->message}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="comment">Content:</label>
                            <textarea class="form-control" rows="10" id="comment" name="relay_content"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

