<div class="justify-content-between footer-wrapper contact-item ">
    <span class="lnr lnr-facebook"></span>
    <span class="lnr lnr-instagram"></span>
    <span class="lnr lnr-twitter"></span>
    <span class="lnr lnr-map"></span>
    <div>SAKURA ECOLOGY --- Copyright 2019</div>
</div>
<div class="icon-main desktop ">
    <a href="javascript:" id="icon-side" class="icon-side" role="button" style="display: inline;" data-toggle="modal"
       data-target="#modalYT">
        <div class="icon-inside">
            <img src="{{asset('/img/icon/icon1.png')}}" alt="">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
        </div>
    </a>
    <a href="javascript:" id="back-to-top" class="back-to-top " role="button">
        <i class="mdi mdi-chevron-up"></i>
    </a>
</div>

<!--Modal: modalYT-->
<div class="modal fade" id="modalYT" tabindex="-1" role="dialog"
     aria-hidden="true">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog modal-lg vertical-align-center" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Body-->
                <div class="modal-body mb-0 p-0">
                    <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/oRvyulJcBW4"
                                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                allowfullscreen></iframe>
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center flex-column flex-md-row">
                    <span class="mr-4">Spread the word!</span>
                    <div>
                        <a class="btn-floating btn-sm btn-fb" href="">
                            <i class="fa fa-facebook-square"></i>
                        </a>
                        <!--Twitter-->
                        <a class="btn-floating btn-sm btn-tw" href="">
                            <i class="fa fa-google"></i>
                        </a>
                        <!--Google +-->
                        <a class="btn-floating btn-sm btn-gplus" href="">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <!--Linkedin-->
                    </div>
                    <button class="btn btn-outline-primary btn-rounded btn-md ml-4"
                            data-dismiss="modal">Close
                    </button>

                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
</div>
<!--Modal: modalYT-->