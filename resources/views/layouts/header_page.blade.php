<header>
    <div id="header-wrapper" class=" header-wrapper" role="main">
        <!-- Top navigation bar -->
        <div class=" row-header">
            <!-- navbar -->
            <nav class="navbar navbar-expand-lg navbar-light justify-content-center ">

                <div class="row align-items-center navbar-toggler  ">
                    <!-- Toggle menu button -->
                    <div data-toggle="slide-collapse" data-target="#top_navbar"
                         aria-controls="top_navbar" >
                        <button type="button" class="btn navbar-toggler">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <!-- End toggle menu button -->
                    <!-- Logo -->
                    <div class="logo-wrapper d-flex align-items-center">
                        <a href="{{url('/')}}" class="go-home">
                            <img src="{{asset('/img/page/logo.png')}}" alt="Logo">
                        </a>
                    </div>
                    <!-- End Logo -->
                </div>
                <div class="row align-items-center justify-content-between content-main">
                    <div class="col-3 logo-desktop">
                        <a href="{{url('/')}}" class="go-home">
                            <img src="{{asset('/img/page/logo.png')}}" alt="Logo">
                        </a>

                    </div>
                    <!-- End btn office tours -->
                    <div class="mobile-nav-bar collapse navbar-collapse justify-content-lg-end" id="top_navbar">

                        <ul class="nav navbar-nav justify-content-between">
                            <li class="nav-item active home">
                                <a class="nav-link" href="{{url('/')}}">
                                    {{__('home.home')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/')}}">
                                    {{__('home.about')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/')}}">
                                    {{__('home.service')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/')}}">
                                    {{__('home.customer')}}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/')}}">
                                    {{__('home.contact')}}
                                </a>
                            </li>
                            <li class="nav-item  languages " style="position: relative">
                                @if (session('lang')=='jp' )
                                    <button type="button" class=" dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{asset('/img/icon/vn.png')}}" alt="Japan">{{__('home.jp')}}
                                    </button>
                                    <div class="dropdown-menu ">
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'vi']) }}"><img src="{{asset('/img/icon/vn.png')}}" alt="">{{__('home.vi')}}</a>
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'en']) }}"><img src="{{asset('/img/icon/english.jpg')}}" alt=""> {{__('home.en')}}</a>
                                    </div>
                                @elseif (session('lang')=='vi')
                                    <button type="button" class=" dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{asset('/img/icon/vn.png')}}" alt="Vietnam">{{__('home.vi')}}
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'jp']) }}" ><img src="{{asset('/img/icon/Japan-Flag-icon.png')}}" alt="">{{__('home.jp')}}</a>
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'en']) }}"><img src="{{asset('/img/icon/english.jpg')}}" alt="">{{__('home.en')}} </a>
                                    </div>
                                @else
                                    <button type="button" class=" dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{asset('/img/icon/english.jpg')}}" alt=""> {{__('home.en')}}
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'vi']) }}"><img src="{{asset('/img/icon/vn.png')}}" alt="">{{__('home.vi')}} </a>
                                        <a class="dropdown-item" href="{{ route('lang',['lang' => 'jp']) }}" ><img src="{{asset('/img/icon/Japan-Flag-icon.png')}}" alt=""> {{__('home.jp')}}</a>
                                    </div>
                                @endif
                            </li>
                            <!-- 360 for mobile -->
                            <li class="nav-item d-none d-xs-block d-sm-block d-md-none d-flex">
                                <a class="nav-link office-tours" href="#">
                                    <img src="{{asset('/img/page/logo.png')}}" class="img-fluid" alt="Logo Mobile"/>
                                </a>
                            </li>
                            <!-- End 360 for mobile -->
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="menu-overlay" style="display: none;"></div>
            <!-- End navbar -->
        </div>

        <!-- End navigation bar -->
    </div>
</header>
