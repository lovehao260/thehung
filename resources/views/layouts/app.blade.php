<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('/img/page/favicon.png')}}" sizes="32x32">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TheHung') }}</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!--Owl Slider -->
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('lib/OwlCarousel2/owl.theme.default.min.css')}}">
    <!-- Font Awesome Icons -->

    <!-- Style css -->
    <link rel="stylesheet" href="{{asset('css/home.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/repository.css')}}">
    <!-- Style css -->
    <script>(function (d, s, id) {
            let js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v3.2&appId=2488139114537079&autoLogAppEvents=1';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146851053-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-146851053-1');
    </script>
</head>
<body class="home">
<div class="wrapper">
    @yield('content')
</div>


<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap -->
<script src="{{asset('lib/bootstrap/js/bootstrap.js')}}"></script>
<script src="{{ asset('js/page.js') }}"></script>
<script>
    $(".reel-grid").css({
        "background-image": "url({{asset('img/page/sl2.jpg')}})",
    });
    $('.ytb-close').click(function () {
        $('.popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
    });
    $(".house").click(function () {
        let html = '  <iframe class="popup-youtube-player" width="640" height="360" src="http://www.youtube.com/embed/' + $(this).data("modal") + '?enablejsapi=1&version=3&playerapiid=ytplayer"' +
            ' frameborder="0" allowfullscreen="true" allowscriptaccess="always"></iframe>';
        $('.embed-responsive').html(html)
    });
    $(".house").hover(function () {
        $this = $(this);
        $('.ytb-close').click(function () {
            $('.popup-youtube-player')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
        })
        if ($(".house:hover").length == 1) {
            $(".reel-grid").css({
                "background-image": "url(" + $(this).data("bg") + ")",
            });
            $(this).addClass('color1');
        } else {
            $('.color-active').hide();
            $(this).show();
        }
    });
    $(".color").hover(function () {

        if ($(".color:hover").length == 1) {
            $('.color-active').show();
            $(this).hide();

        } else {
            $('.color-active').hide();
            $(this).show();
        }

    });

</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">


    $(document).ready(function(){
        let CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let link ="{{ url('/project') }}";
        $('.typeahead').keyup(function(){
            let query = $(this).val();

            if(query  != ""){
                $.ajax({
                    url:"{{ route('autocomplete') }}",
                    method:"POST",
                    data:{query:query, _token:CSRF_TOKEN},
                    success:function(data){
                        let data_s =[];
                        data.forEach(function(item){
                            let html =' <a href="'+link+'/'+item.slug+'">'+item.tittle_vn+'</a>'
                            data_s.push(html);
                        });
                        $('#auto-list').html(data_s)
                    }
                });
            }else{
                $('#auto-list').html('')
            }
        });
    });

</script>
@stack('page-scripts')
</body>
</html>
