@extends('layouts.app')
@section('content')
    <header class="header header-top ">
        <div id="header-wrapper" class=" header-wrapper" role="main">
            <!-- Top navigation bar -->
            <div class=" row-header">
                <!-- navbar -->
                <nav>
                    <div class="row menu-header animated fadeInDown">
                        <div class="col-lg-3 col-12 icon-logo">
                            <!-- Toggle menu button -->
                            <div data-toggle="slide-collapse" data-target="#top_navbar">
                                <button type="button" class="btn navbar-toggler main-icon">
                                    <div id="nav-icon1">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </button>
                            </div>
                            <div class="logo-menu">
                                <a href="{{url('/')}}" class="logo">
                                    <img src="{{asset('img/page/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End btn office tours -->
                        <div class="col-lg-9 col-sm-12" id="top_navbar">
                            <ul class="menu">
                                <li class="active"><a href="{{url('/')}}">Our Work</a></li>
                                <li><a href="{{url('/reels')}}">Reels</a></li>
                                <li><a href="{{url('/contacts')}}">Contact</a></li>
                                <li>

                                    <form action="/action_page.php" method="get">
                                        <input class="typeahead" placeholder="search key...">
                                        <i class="fa fa-search search"></i>
                                        <div id="auto-list">

                                        </div>
                                    </form>
                                </li>

                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- End navbar -->
            </div>

            <!-- End navigation bar -->
        </div>
    </header>
    <section class="landing" style=" background-position: center center;">

        <div class="video-wrapper">
            <video loop="" muted="" autoplay="" playsinline="">
                <source
                        src="{{asset('img/video/slider.mp4')}}"
                        type="video/mp4">
            </video>
            <div class="overlay"></div>
        </div>
        <div class="content-wrap">
            <article class="entry">
                <div class="animated bounceInDown"><h2>Carbon is a full-service creative studio specializing in visual
                        effects, motion graphics, and design.</h2></div>
            </article>
        </div>
        <div class="meta">
            <ul>
                <li><a href="{{url('contacts')}}">Ha Noi</a></li>
                <li><a href="#">Home Page</a></li>
                <li><a href="{{url('contacts')}}">Ho Chi Minh</a></li>
            </ul>
            <div class="banner-icon">
                <a href="#work-grid" class="arrow-down no-fade" aria-label="Work grid">
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="19" viewBox="0 0 11.29 18.19"
                         src="https://carbonvfx.com/wp-content/themes/carbon-vfx-wordpress/images/arrow-down.svg"
                         class="svg"
                         alt="Down arrow">
                        <polygon
                                points="11.29 12.55 10.59 11.84 6.15 16.28 6.15 0 5.15 0 5.15 16.28 0.71 11.84 0 12.55 5.65 18.19 11.29 12.55"
                                fill="#fff"></polygon>
                    </svg>
                </a>
            </div>
        </div>
    </section>
    <section class="list-content animated bounceInDown">
        <div class="content">
            <div class="menu-overlay" style="display: none"></div>
            <div class="row">
{{--                <div class="ml-auto bg-primary">--}}
{{--                    <datalist id="auto-list">--}}
{{--                    </datalist>--}}
{{--                </div>--}}
                @foreach($new as $key=>$value)
                    @if($key ==0)
                        <div class="col-md-7 col-12">
                @elseif($key ==1)
                                <div class="col-md-5 col-12">
                        @elseif($key ==2)
                                        <div class="col-md-4 col-12">
                            @elseif($key ==3)
                                                <div class="col-md-4 col-12">
                                @elseif($key ==4)
                                                        <div class="col-md-4 col-12">
                                    @elseif($key ==5)
                                                                <div class="col-md-6 col-12">
                                        @elseif($key ==6)
                                                                        <div class="col-md-6 col-12">
                                            @elseif($key ==7)
                                                                                <div class="col-md-5 col-12">
                                                @elseif($key ==8)
                                                                                        <div class="col-md-7 col-12">
                                                    @else
                                                                                                <div class="col-md-4 col-12">
                             @endif

                            <div class="list"
                                 style="background-image: url({{asset('img/upload/'.$value->avatar)}});  ">
                                <div class="content-overlay"></div>
                                <h3 class="title-wrap ">
                                    <span>{{$value->tittle_vn}}</span>

                                </h3>
                                <div class="preview fadeIn-bottom">
                                    <!-- Button trigger modal -->
                                    <i class="fa fa-play play-icon video-home"
                                       href="javascript:"
                                       id="icon-side"
                                       role="button"
                                       style="display: inline;"
                                       data-video="{{asset('img/video/'.$value->video)}}"
                                       data-toggle="modal"
                                       data-target="#modalYT">
                                        <span class="play-icon">QUICK PLAY</span>
                                    </i>
                                    <i class="fa fa-eye view-pr">
                                        <span><a href="{{url('/project/'.$value->slug)}}">VIEW PROJECT</a></span>
                                    </i>
                                </div>
                            </div>
                        </div>
                        @endforeach


            </div>
        </div>


        <footer class="footer ">
            <span data-wow-delay="0.2s" class="span3 wow rollIn"
                  style="visibility: visible; animation-delay: 0.2s; animation-name: rollIn;">
                 <i class="fa fa-facebook-square"></i>
            </span>
            <span data-wow-delay="0.6s" class="span3 wow rollIn"
                  style="visibility: visible; animation-delay: 0.6s; animation-name: rollIn;">
                 <i class="fa fa-google"></i>
            </span>
            <span data-wow-delay="1s" class="span3 wow rollIn"
                  style="visibility: visible; animation-delay: 1s; animation-name: rollIn;">
                 <i class="fa fa-instagram"></i>
            </span>

            <!--Linkedin-->
        </footer>
    </section>

    <!--Modal: modalYT-->
    <div class="modal fade" id="modalYT" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-lg vertical-align-center" role="document">
                <!--Content-->
                <div class="modal-content">
                    <!--Body-->
                    <div class="modal-body mb-0 p-0">
                        <div class="embed-responsive show-video embed-responsive-16by9 z-depth-1-half">

                        </div>
                    </div>
                    <!--Footer-->
                    <div class="modal-footer justify-content-center flex-column flex-md-row">
                        <span class="mr-4">Spread the word!</span>
                        <div>
                            <a class="btn-floating btn-sm btn-fb" href="">
                                <i class="fa fa-facebook-square"></i>
                            </a>
                            <!--Twitter-->
                            <a class="btn-floating btn-sm btn-tw" href="">
                                <i class="fa fa-google"></i>
                            </a>
                            <!--Google +-->
                            <a class="btn-floating btn-sm btn-gplus" href="">
                                <i class="fa fa-instagram"></i>
                            </a>
                            <!--Linkedin-->
                        </div>
                        <button class="btn btn-outline-primary btn-rounded btn-md ml-4"
                                data-dismiss="modal">Close
                        </button>

                    </div>
                </div>
                <!--/.Content-->
            </div>
        </div>
    </div>
    <!--Modal: modalYT-->
@endsection


