@extends('layouts.app')
@section('content')

    <header class="header header-detail ">
        <div id="header-wrapper" class=" header-wrapper" style="background-color:transparent" role="main">
            <!-- Top navigation bar -->
            <div class=" row-header">
                <!-- navbar -->
                <nav>
                    <div class="row menu-header animated fadeInDown">
                        <div class="col-lg-3 col-12 icon-logo">
                            <!-- Toggle menu button -->
                            <div data-toggle="slide-collapse" data-target="#top_navbar">
                                <button type="button" class="btn navbar-toggler main-icon">
                                    <div id="nav-icon1">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </div>
                                </button>
                            </div>
                            <div class="logo-menu">
                                <a href="{{url('/')}}" class="logo">
                                    <img src="{{asset('img/page/logo.png')}}" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- End btn office tours -->
                        <div class="col-lg-9 col-sm-12" id="top_navbar">
                            <ul class="menu">
                                <li ><a href="{{url('/')}}">Our Work</a></li>
                                <li class="active"><a href="{{url('/reels')}}">Reels</a></li>
                                <li><a href="{{url('/contacts')}}">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <!-- End navbar -->
            </div>

            <!-- End navigation bar -->
        </div>
    </header>
    <div class="menu-overlay" style="display: none ; background-color: white ; opacity: 1"></div>
    <div class="reel-grid animated fadeIn">
        <nav>
            <ul class="reel-item animated bounceInUp">
                <li class="house  " data-bgcolor="#A2DED0"
                    data-bg="{{asset('img/page/sl2.jpg')}}"
                    data-modal="cM7pn99GvW8"
                >
                    <a href="javascript:" id="icon-side" role="button" style="display: inline;"
                       data-toggle="modal"
                       data-target="#modalYT">
                        <span>2020</span>
                    </a>
                </li>
                <li class="color animated">
                    <a href="#">
                        <span>YEAR</span>
                    </a>
                </li>
                <div class="color-active">
                    <li class="house span1" data-bgcolor="#DCC6E0"
                        data-bg="{{asset('img/page/sl1.jpg')}}"
                        data-modal="q22GQRcbmP4">
                        <a href="javascript:" role="button" style="display: inline;"
                           data-toggle="modal"
                           data-target="#modalYT">
                            <span class="">2021</span>
                        </a>
                    </li>
                    <li class=" house" style="margin-top: -40px" data-bgcolor="#FDE3A7"
                        data-bg="{{asset('img/page/sl3.png')}}"
                        data-modal="GNCGH2JNlmA">
                        <a href="javascript:" role="button" style="display: inline;"
                           data-toggle="modal"
                           data-target="#modalYT">
                            <span>2019</span>
                        </a>
                    </li>
                </div>
            </ul>
        </nav>
    </div>

    <!--Modal: modalYT-->
    <div class="modal fade " id="modalYT" tabindex="-1" role="dialog"
         aria-hidden="true">
        <div class="modal-footer ">
            <i class="fa fa-close ytb-close" data-dismiss="modal"></i>
        </div>
        <div class="vertical-alignment-helper">
            <div class="modal-dialog modal-lg vertical-align-center" role="document">
                <!--Content-->
                <div class="modal-content ">
                    <!--Body-->
                    <div class="modal-body mb-0 p-0">
                        <div class="embed-responsive embed-responsive-16by9 z-depth-1-half">

                        </div>
                    </div>

                </div>
                <!--/.Content-->
            </div>
        </div>
    </div>

@endsection

