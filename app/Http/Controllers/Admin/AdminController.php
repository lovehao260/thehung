<?php

namespace App\Http\Controllers\Admin;

use App\CategoryModel;
use App\Mail\RelayMailContract;
use App\Permission;
use App\Role;
use App\Services\AdminService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\UserRoles;


class AdminController extends Controller
{
    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->middleware('auth');
        $this->adminService = $adminService;
    }

    //DashBoard --------------------------------------------------------------------------------------------------------
    public function index()
    {
        $users = DB::table('users')->count();
        $news = DB::table('news')->where('del_flg', 0)->count();
        $contacts = DB::table('contacts')->count();
        $contacts_dont = DB::table('contacts')->where('status', 0)->count();
        $contacts_seen = DB::table('contacts')->where('status', 1)->count();
        $contacts_mail = DB::table('contacts')->where('status', 2)->count();
        return view('admin/layouts.index', [
            'count_users' => $users,
            'count_news' => $news,
            'count_contacts' => $contacts,
            'contacts_dont' => $contacts_dont,
            'contacts_seen' => $contacts_seen,
            'contacts_mail' => $contacts_mail,
        ]);
    }

    public function dashBoard()
    {
        //retrieve visitors and pageview data for the current day and the last seven days
        $analyticsData = Analytics::fetchVisitorsAndPageViews(Period::days(7));
        //send these two variables to your view
        $dates = $analyticsData->pluck('date');
        $visitors = $analyticsData->pluck('visitors');
        return view('admin/layouts.index', [
            'visitors' => $analyticsData,
        ]);
    }


    // USer Manage------------------------------------------------------------------------------------------
    public function user()
    {
        return view('admin/user.index');
    }

    //User add user
    public function getAddUser(Request $request)
    {
        if ($request->user()->can('create-users')) {
            $role = DB::table('permissions')->get();
            return view('admin/user.add', [
                'role' => $role,
            ]);
        }
        return redirect('admin/user')->with('status', trans('admin/layout.role_log'));
    }

    public function postAddUser(Request $request)
    {
        $validator = $this->adminService->ValidateUser($request);
        if ($validator->fails()) {
            return redirect('admin/user-add')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->adminService->addUser($request);
            return redirect('admin/user')->with('status', trans('admin/layout.add_sucss'));
        }
    }

    public function getEditUser(Request $request, $id)
    {
        if ($request->user()->can('edit-users')) {
            $user_data = User::find($id);
            $role = DB::table('permissions')->get();
            $per_id = DB::table('users_permissions')->where('user_id', $id)->first();
            return view('admin/user.edit', [
                'user_data' => $user_data,
                'role' => $role,
                'per_id' => $per_id
            ]);
        }
        return redirect('admin/user')->with('status', trans('admin/layout.role_log'));
    }

    public function postEditUser(Request $request)
    {
        $validator = $this->adminService->ValidateEditUser($request);
        if ($validator->fails()) {
            return redirect('admin/user-edit/' . $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->adminService->editUser($request);
            return redirect('admin/user')->with('status', trans('admin/layout.edit_sucss'));
        }
    }

    public function getEditUserPass(Request $request, $id)
    {
        if ($request->user()->can('password-users')) {
            return view('admin/user.edit_password', [
                'id' => $id
            ]);
        }
        return redirect('admin/user')->with('status', trans('admin/layout.role_log'));
    }

    public function postEditUserPass(Request $request)
    {
        $validator = $this->adminService->ValidateUserPass($request);
        if ($validator->fails()) {
            return redirect('admin/user-edit-password/' . $request->id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->adminService->editUserPass($request);
            return redirect('admin/user')->with('status', trans('admin/layout.edit_sucss'));
        }
    }

    // Delete User
    public function postDeleteUser(Request $request)
    {
        if ($request->user()->can('delete-users')) {
            $request = request();
            $this->adminService->deleteUser($request);
            return 0;
        }
        return 0;
    }

    public function roleUser(Request $request)
    {
        $this->adminService->editRole($request);
        return 1;
    }

    public function getRoles()
    {
        return view('admin/roles.index');
    }

    public function getAddRoles(Request $request)
    {
        if ($request->user()->can('create-roles')) {
            $permission = config('systemsetting.permission');
            return view('admin/roles.add')->with('permission', $permission);
        }
        return redirect()->back()->with('status', trans('admin/layout.role_log'));

    }

    public function postAddRoles(Request $request)
    {
        $this->adminService->addPermission($request);
        return redirect('admin/roles')->with('status', trans('admin/layout.edit_sucss'));
    }

    public function deleteRoles(Request $request)
    {
        if ($request->user()->can('delete-roles')) {
            $request = request();
            $id = $request->Id;
            $roles = Permission::find($id);
            $roles->delete();
            return 0;
        }
        return 0;

    }

    public function getEditRoles(Request $request, $id)
    {
        if ($request->user()->can('edit-roles')) {
            $data = Permission::find($id);
            $slug = json_decode($data->slug);
            $permission = config('systemsetting.permission');
            return view('admin/roles.edit', [
                'data' => $data,
                'permission' => $permission,
                'slug' => $slug,
            ]);
        }
        return redirect()->back()->with('status', trans('admin/layout.role_log'));

    }

    public function postEditRoles(Request $request)
    {
        $this->adminService->editPermission($request);
        return redirect('admin/roles')->with('status', trans('admin/layout.edit_sucss'));
    }

    //Contact --------------------------------------------------------------------------------------------------------
    public function getContact()
    {
        return view('admin/contact.index');
    }


    public function deleteContact()
    {
        try {
            $request = request();
            if ($request->user()->can('delete-tasks')) {
                $this->adminService->deleteContact($request);
            }
            return 0;
        } catch (\Exception $exception) {
            return view('errors.500');
        }
    }

    public function detailContact($id)
    {
        $details = $this->adminService->detailContact($id);
        return view('admin/contact.detail', [
            'details' => $details
        ]);
    }

    public  function downLoadFile(Request $request){

        //PDF file is stored under project/public/download/info.pdf
        $path = "img/contact/".$request->file_contact;
        return response()->download(public_path($path));
    }
    public function relayMail(Request $request)
    {
        $admin_email = $this->adminService->emailAdmin();
        if ($admin_email != null) {
            Mail::to($request->email)
                ->cc($admin_email)
                ->send(new RelayMailContract($request));
            $this->adminService->relayContact($request);
        } else {
            Mail::to($request->email)
                ->send(new RelayMailContract($request));
            $this->adminService->relayContact($request);
        }
        return redirect('/admin/contact')->with('status', 'Send mail success ');
    }


}
