<?php

namespace App\Http\Controllers\Admin;

use App\CategoryModel;
use App\Services\PostsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{

    protected $postsService;

    public function __construct(PostsService $postsService)
    {
        $this->middleware('auth');
        $this->postsService = $postsService;
    }

    //Category --------------------------------------------------------------------------------------------------------
    public function categories()
    {
        return view('admin/category.index');
    }

    public function getAddCategory(Request $request)
    {

        $categories = $this->postsService->parentCategory();
        return view('admin/category.add')->with('categories', $categories);

        return redirect('admin/categories')->with('status', trans('admin/layout.role_log'));
    }

    public function postAddCategory(Request $request)
    {

        $validator = $this->postsService->validateAddCategories($request);
        if ($validator->fails()) {
            return redirect('admin/categories-add')->withErrors($validator)->withInput();
        } else {
            $this->postsService->addCategory($request);
            return redirect('admin/categories')->with('status', trans('admin/layout.add_sucss'));
        }
    }

    public function getEditCategory(Request $request)
    {
        $categories = $this->postsService->parentCategory();
        $data = $this->postsService->getEditCategories($request);
        return view('admin/category.edit', [
            'categories' => $categories,
            'data' => $data,
        ]);
    }

    public function postEditCategory(Request $request)
    {
        $validator = $this->postsService->validateAddCategories($request);
        if ($validator->fails()) {
            return redirect('admin/categories-edit/' . $request->id)
                ->withErrors($validator)
                ->withInput();
            {
            }
        } else {
            $this->postsService->editCategories($request);
            return redirect('admin/categories')->with('status', trans('admin/layout.add_sucss'));
        }
    }

    public function postDeleteCategory()
    {
        try {
            $request = request();
            $this->postsService->deleteCategories($request);
            return 1;

        } catch (\Exception $exception) {
            return view('errors.500');
        }
    }

    //News --------------------------------------------------------------------------------------------------------
    public function new()
    {
        return view('admin/new.index');
    }

    // Add new
    public function getAddNew(Request $request)
    {
        return view('admin/new.add');

    }

    public function postAddNew(Request $request)
    {
        $validator = $this->postsService->validateAddParams($request);
        if ($validator->fails()) {
            return redirect('admin/new-add')->withErrors($validator)->withInput();
        } else {
            $this->postsService->addNew($request);
            return redirect('admin/news')->with('status', trans('admin/layout.add_sucss'));
        }
    }

    // Delete New
    public function postDeleteNew()
    {
        $request = request();
        $this->postsService->deleteNew($request);
        return 0;
    }

    // Edit New
    public function getEditNew(Request $request, $id)
    {
        $new_data = $this->postsService->editNewById($id);
        return view('admin/new.edit', [
            'new_data' => $new_data
        ]);

    }

    public function postEditnew(Request $request)
    {
        $id = $request->id;
        $validator = $this->postsService->validateEditParams($request);
        if ($validator->fails()) {
            return redirect('admin/new-edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->postsService->editNew($request);
            return redirect('admin/news')->with('status', trans('admin/layout.edit_sucss'));
        }
    }

    public function postNewsHot(Request $request)
    {
        $this->postsService->hotNews($request);
        return 1;
    }

}
