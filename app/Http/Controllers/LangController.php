<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
class LangController extends Controller
{
    private $langActive = [
        'vi',
        'en',
        'jp'
    ];
    private $langAdmin = [
        'jp',
        'en',

    ];
    public function lang(Request $request, $lang)
    {

        if (in_array($lang, $this->langActive)) {
            $request->session()->put(['lang' => $lang]);
            return redirect()->back();
        }
    }
    public function AdminLang(Request $request, $lang_ad)
    {
        if (in_array($lang_ad, $this->langAdmin)) {
            $request->session()->put(['lang_admin' => $lang_ad]);
            return redirect()->back();
        }

    }
}
