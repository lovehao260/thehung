<?php

namespace App\Http\Controllers;

use App\Jobs\SendThanksMail;
use App\Mail\ThanksYouMail;
use App\NewModel;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Lang;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;

class HomeController extends Controller
{

    protected $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }


    public function index()
    {

        $new = DB::table('news')
            ->where('news.del_flg', '0')
            ->orderBy('created_at', 'ASC')->get();
        $i = 0;
        return view('home', [
            'new' => $new,
            'i' => $i
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */
    public function autocomplete(Request $request)
    {
        if($request->query != null){
            $data = NewModel::select("tittle_vn","slug")
                ->where("tittle_vn","LIKE","%{$request->input('query')}%")
                ->get();
            return response()->json($data);
        }
    }
    public function reels()
    {
        return view('reels');
    }

    public function contacts()
    {
        return view('contact');
    }

    function businessOutline()
    {
        return view('business_outline');
    }

    public function project($slug)
    {
        $news = NewModel::where('slug', $slug)->first();
        $news_hot = $this->adminService->newsHot();
        return view('project', [
            'news' => $news,
            'news_hot' => $news_hot
        ]);

    }

    public function postContact(Request $request)
    {
        //chuyển thời gian về giây.
        $time = 30;
        $validator = $this->adminService->validateContact($request);
        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)
                ->withInput();
        } else {
            $this->adminService->addContact($request);
            alert()->success(Lang::get('messages.success'), Lang::get('messages.success_mail'));
            return redirect()->back();
        }
    }


}

