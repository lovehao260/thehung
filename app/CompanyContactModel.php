<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyContactModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ="company_contact";
    protected $fillable = [
        'code_company', 'phone','address','mail','fax','hotline','created_at','updated_at'
    ];
    public $timestamps= true;
    public function catalog(){
        return $this->belongsTo('App\CompanyModel');
    }
}
