<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ="company";
    protected $fillable = [
        'name', 'logo','code','active','mail_footer_en','mail_footer_jp','auto_relay','created_at','updated_at'
    ];
    public $timestamps= true;
    public function  product(){
        return $this->hasMany('App\CompanyContactModel','code_company','code');
    }
}
