<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Permission;
use Illuminate\Support\Facades\Gate;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Permission::get()->map(function($permission){
            $data =json_decode($permission->slug);
            if (is_array($data) || is_object($data))
            {
                foreach ($data as $value)
                {
                    Gate::define($value, function($user) use ($permission){
                        return $user->hasPermission($permission);
                    });
                }
            }
            return false;
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
