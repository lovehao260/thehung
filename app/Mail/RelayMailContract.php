<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class RelayMailContract extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->content = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $company = DB::table('company')
            ->where('company.active', 1)
            ->get();
        foreach ($company as $row){
            $company_contact = DB::table('company')
                ->leftJoin('company_contact', 'company.code', '=', 'company_contact.code_company')
                ->where('company.active', 1)
                ->get();
            $stt=1;
               return $this->view('mails.contact_relay_mail')
                ->with([
                    'content' => $this->content,
                    'company'=>$row,
                    'company_contact'=>$company_contact,
                    'stt'=>$stt
                ]);
        }
    }
}
