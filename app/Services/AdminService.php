<?php

namespace App\Services;
use Illuminate\Http\Request;

interface AdminService
{
    /**
     * @return string
     */

    function getUser();
    function getContact();
    function chartNew();
    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */

    function ValidateContact($request);
    /**
     * @param $request Request
     * @return string
     */
    function ValidateUser($request);

    function addUser($request);
    function ValidateEditUser($request);
    function ValidateUserPass($request);
    function editUser($request);
    function editUserPass($request);
    function addContact($request);



    function deleteContact($request);
    /**
     * @param $request Request
     * @return string
     */
    function deleteUser($request);

    /**
     * @param $id
     * @return string
     */
    function detailContact($id);
    function relayContact($request);
    /**
     * @return string
     */
    function emailAdmin();
    /**
     * @param $request Request
     * @return string
     */
    function editRole($request);

    /**
     * @return string
     */
    function newsHot();
    /**
     * @param $request Request
     * @return string
     */
    function addPermission($request);
    function editPermission($request);




}
