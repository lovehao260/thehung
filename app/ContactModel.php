<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ="contacts";
    protected $fillable = [
        'name', 'email','message','relay_content','status'
    ];
    public $timestamps= true;
}
