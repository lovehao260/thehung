<?php

namespace App\ServiceImpl;

use App\ContactModel;
use App\Services\AdminService;
use App\User;
use App\UserRoles;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Lang;
use App\Permission;

class AdminServiceImpl implements AdminService
{
    /**
     * Show all of the users for the application.
     */
    function __construct()
    {
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put('ip', request()->ip());
            $activity->properties = $activity->properties->put('browser', request()->header('User-Agent'));
        });
    }


    function getUser()
    {
        $query = DB::table('users')
            ->get()->toArray();
        return datatables($query)->make(true);
    }

    function getRoles(){
        $query = DB::table('permissions')
            ->get()->toArray();
        return DataTables::of($query)
            ->addColumn('created', function ($data) {
                return date_from_database($data->updated_at, 'd-m-Y');
            })
            ->rawColumns(['created'])
            ->make(true);
    }

    /**
     * @return array
     */
    function chartNew()
    {
        $year = now()->year;
        $query = DB::table('news')
            ->where('news.del_flg', '0')
            ->select(DB::raw('MONTH(created_at) as mouth, YEAR(created_at) as year'), DB::raw('count(*) as views'))
            ->whereYear('created_at', '=', $year)
            ->groupBy('mouth', 'year')
            ->get();

        $usermcount = [];
        $userArr = [];
        foreach ($query as $key => $value) {
            $usermcount[(int)$value->mouth] = $value->views;
        }
        for ($i = 1; $i <= 12; $i++) {
            if (!empty($usermcount[$i])) {
                $userArr[$i] = $usermcount[$i];
            } else {
                $userArr[$i] = 0;
            }
        }
        return $userArr;
    }

    //User------------------------------------------------------------------------------------------------------------

    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateUser($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'cf_password' => 'required|min:6|same:password'
        ]);
        return $validator;
    }

    function ValidateUserPass($request)
    {
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'cf_password' => 'required|min:6|same:password'
        ]);
        return $validator;
    }

    /**
     *
     * @param $request Request
     */
    function addUser($request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
        $dev_create_perm = Permission::where('id',$request->permission)->first();
        $user->permissions()->attach($dev_create_perm);
        activity()
            ->causedBy(Auth::user())
            ->performedOn($user)
            ->withProperties($request)
            ->log('Add User User');
    }

    function ValidateEditUser($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => [
                'required',
                Rule::unique('users')->ignore($request->id),
            ],
            'email' => [
                'required',
                Rule::unique('users')->ignore($request->id),
            ],
        ]);
        return $validator;
    }

    function editUser($request)
    {

        $user = User::find($request->id);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->save();
        UserRoles::where('user_id', '=', $request->id)->delete();
        $dev_create_perm = Permission::where('id',$request->permission)->first();
        $user->permissions()->attach($dev_create_perm);
        activity()
            ->causedBy(Auth::user())
            ->performedOn($user)
            ->withProperties($request)
            ->log('Edit User with ID' . $user->name);
    }

    function editUserPass($request)
    {
        $user = User::find($request->id);
        $user->password = Hash::make($request->password);
        $user->save();

        activity()
            ->causedBy(Auth::user())
            ->performedOn($user)
            ->withProperties($request)
            ->log('Edit Password User with ID' . $user->name);
    }

    /**
     *
     * @param $request Request
     */

    function deleteUser($request)
    {
        $id = $request->Id;
        $user = User::find($id);
        $user->delete();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($user)
            ->withProperties($request)
            ->log('Delete  User with ID' . $user->name);
    }

    function editRole($request)
    {
        $id = $request->id;
        $user = User::A($id);
        $user->role = $request->role;
        $user->save();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($user)
            ->withProperties($request)
            ->log('Edit Role User with ID' . $id);
    }

    /**
     * Add permission
     * @param $request Request
     * @return null
     */
    function addPermission($request){
        $data= new Permission();
        $data->name=$request->name;
        $data->slug= json_encode ($request->flags);
        $data->description=$request->description;
        $data->created_by=Auth::user()->name;
        $data->save();
    }

    /**
     * Edit permission
     * @param $request Request
     * @return null
     */
    function editPermission($request){
        $data=  Permission::find($request->id);;
        $data->name=$request->name;
        $data->slug= json_encode ($request->flags);
        $data->description=$request->description;
        $data->save();
    }
//Contact-------------------------------------------------------------------------------------------------------

    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateContact($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'required|min:6',
            'file_contact' => 'mimes:jpeg,jpg,png,docx,pdf,txt,zip,rar|max:20480' // max 20000kb
        ]);
        return $validator;
    }

    /**
     * @param $request Request
     */
    function addContact($request)
    {
        $contact = new ContactModel();
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->address = $request->address;
        $contact->message = $request->message;
        if (!empty($request->file('file_contact'))) {
            $file = $request->file('file_contact');
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $contact->file_contact = $avatar;
            $request->file('file_contact')->move('img/contact', $avatar);
            $contact->save();
        }
        $contact->save();

    }

    function getContact()
    {
        $query = DB::table('contacts')->where('isRecruit',false)
            ->get()->toArray();
        return DataTables::of($query)
            ->addColumn('status', function ($data) {
                if ($data->status === 0) {
                    return '<i class="fa fa-eye-slash">'.trans('admin/layout.not_seen') .'</i>';
                } else if ($data->status === 1) {
                    return '<i class="fa fa-eye"> '.trans('admin/layout.watched') .'</i> ';
                } else {
                    return '<i class="fa fa-mail-reply"> Send mail</i> ';
                }
            })
            ->rawColumns(['status'])
            ->make(true);

    }

    function getContactRelay()
    {
        $query = DB::table('contacts')
            ->where('status', '<>', 0)
            ->get();
        return datatables($query)->make(true);
    }

    /**
     * @param $request Request
     *
     */
    function deleteContact($request)
    {
        $id = $request->Id;
        $contacts = ContactModel::find($id);
        $contacts->delete();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($contacts)
            ->withProperties($request)
            ->log('Delete contact with ID' . $contacts->id);
    }

    /**
     * @return object
     */
    function detailContact($id)
    {
        $contact = ContactModel::find($id);
        if ( $contact->status == 0){
            $contact->status = 1;
            $contact->save();
        }
        return $contact;
    }

    /**
     * @param  $request
     */
    function relayContact($request)
    {
        $id = $request->id;
        $contact = ContactModel::find($id);
        $contact->status = 2;
        $contact->relay_content = $request->relay_content;
        $contact->save();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($contact)
            ->withProperties($request)
            ->log('Relay contact with ID' . $id);
    }


    /**
     * @return array
     */
    function emailAdmin()
    {
        $query = DB::table('users')
            ->select('email')
            ->where('role','<>', 1)
            ->get()->toArray();
        return $query;
    }

    function newsHot()
    {
        $news_hot = DB::table('news')
            ->where('news.del_flg', '0')
            ->orderBy('status', 'Desc')
            ->orderBy('created_at', 'ASC')
            ->paginate(5);
        return $news_hot;
    }

    // Activity log-----------------------------------------------------------------------------------
    //Get all Activity Log
    function getLog()
    {
        $query = DB::table('activity_log')
            ->select('users.name', 'activity_log.causer_id', 'activity_log.properties', 'activity_log.description', 'activity_log.created_at')
            ->join('users', 'activity_log.causer_id', '=', 'users.id');
        return datatables($query)->make(true);
    }

}

