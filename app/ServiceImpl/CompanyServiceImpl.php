<?php

namespace App\ServiceImpl;

use App\CompanyContactModel;
use App\CompanyModel;
use App\Services\CompanyService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;


class CompanyServiceImpl implements CompanyService
{

    // Company Contact handling --------------------------------------------------------------------------------
    function companyCode()
    {
        $query = DB::table('company')->select('id', 'code', 'name')->get();
        return $query;
    }

    function getCompany()
    {
        $query = DB::table('company')
            ->get();

        return datatables($query)->make(true);
    }

    function validateCompany($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100|unique:company',
            'code' => 'required|unique:company|max:10',
            'mail_footer_jp' => 'required|max:255',
            'mail_footer_en' => 'required|max:255',
            'logo' => 'mimes:jpeg,jpg,png|required|max:20000' // max 20000kb
        ]);
        return $validator;
    }

    function validateEditCompany($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:100',
            'code' => [
                'required',
                Rule::unique('company')->ignore($request->id),
                'max:10',
            ],
            'mail_footer_jp' => 'required|max:255',
            'mail_footer_en' => 'required|max:255',
            'logo' => 'mimes:jpeg,jpg,png|image|max:20000' // max 20000kb
        ]);
        return $validator;
    }

    function addCompany($request)
    {
        $company = new CompanyModel();
        $company->name = $request->name;
        $company->code = $request->code;
        $company->mail_footer_jp = $request->mail_footer_jp;
        $company->mail_footer_en = $request->mail_footer_en;
        if (!empty($request->file('logo'))) {
            $avatar = $request->file('logo')->getClientOriginalName();
            $radom_name = str_random(6) . $avatar;
            $company->logo = $radom_name;
            $request->file('logo')->move('img/upload', $radom_name);
            $company->save();
        } else {
            echo "Not Found";
        }
        $company->save();

    }

    function editCompanyById($id)
    {
        return CompanyModel::find($id);
    }

    function editCompany($request)
    {
        $id = $request->route('id');
        $company = CompanyModel::find($id);
        // Save edit on table company contact
        $company_contact = CompanyContactModel::where('code_company', '=', $company->code)->get();
        foreach ($company_contact as $row) {
            $row->code_company = $request->code;
            $row->save();
        }
        $company->code = $request->code;
        $company->name = $request->name;
        $company->mail_footer_jp = $request->mail_footer_jp;
        $company->mail_footer_en = $request->mail_footer_en;
        // save file logo
        if (!empty($request->file('logo'))) {
            File::delete('img/upload/' . $company->logo);
            $avatar = $request->file('logo')->getClientOriginalName();
            $radom_name = str_random(6) . $avatar;
            $company['logo'] = $radom_name;
            $request->file('logo')->move('img/upload', $radom_name);
            $company->save();
        } else {
            echo "Not Found";
        }
        $company->save();
      }

    function activeCompany($request)
    {
        $id = $request->id;
        if ($id) {
            $company = CompanyModel::find($id);
            $company->active = !$company->active;
            $company->save();
        }
        return true;
    }

    function deleteCompany($request)
    {
        $id = $request->Id;
        $company = CompanyModel::find($id);
        $query = DB::table('company')
            ->rightJoin('company_contact', 'company.code', '=', 'company_contact.code_company')
            ->where('company_contact.code_company', $company->code)
            ->count();
        if ($company->active == 0 && $query == 0) {
            File::delete('img/upload/' . $company->logo);
            $company->delete();
            return 0;
        } else {
            return 1;
        }
    }

    function companyAutoMail($request){
        $id = $request->id;
        $company = CompanyModel::find($id);
        $company->auto_relay =$request->relay_s;
        $company->save();
    }
    // Company Contact handling -----------------------------------------------------------------------------------
    function getCompanyCt()
    {
        $query = DB::table('company_contact')
            ->get();
        return datatables($query)->make(true);
    }

    function validateCompanyCt($request)
    {
        $validator = Validator::make($request->all(), [
            'code_company' => 'required| max:20',
            'phone' => 'required| max:100',
            'fax' => 'max:100',
            'mail' => 'required|email',
            'address' => 'required',
        ]);
        return $validator;
    }

    function addCompanyCt($request)
    {
        $params = $request->post();
        unset($params['_token']);
        //Re add customCheck
        $company = new CompanyContactModel();
        foreach ($params as $key => $value) {
            $company->$key = $value;
        }
        return $company->save();
    }

    function editCompanyCtById($id)
    {
        return CompanyContactModel::find($id);
    }

    function editCompanyCt($request)
    {
        $id = $request->route('id');
        $company = CompanyContactModel::find($id);
        $params = $request->post();
        unset($params['_token']);
        foreach ($params as $key => $value) {
            $company->$key = $value;
        }
        return $company->save();

    }

    function deleteCompanyCt($request)
    {
        $id = $request->Id;
        $company = CompanyContactModel::find($id);
        $company->delete();

    }

}

