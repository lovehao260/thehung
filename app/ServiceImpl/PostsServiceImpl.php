<?php

namespace App\ServiceImpl;

use App\CategoryDescription;
use App\CategoryModel;
use App\NewModel;
use App\Services\PostsService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\File;

class PostsServiceImpl implements PostsService
{

    //Categories
    function getCategories()
    {

        $query = DB::table('categories')
            ->orderBy('parent_id', 'asc')
            ->orderBy('name_en', 'asc')
            ->get();

        return DataTables::of($query)
            ->editColumn('name', function ($item) {
                return $item->name_en;
            })
            ->addColumn('parent_id', function ($query) {
                if ($query->parent_id == 0) {
                    return "None";
                } else {
                    $parent = DB::table('categories')
                        ->where('id', $query->parent_id)->first();
                    return $parent->name_en ;
                }
            })
            ->editColumn('created_at', function ($item) {
                return date_from_database($item->updated_at, 'd-m-Y');
            })
            ->editColumn('updated_at', function ($item) {
                return date_from_database($item->updated_at, 'd-m-Y');
            })
            ->rawColumns(['parent_id'])
            ->make(true);

    }

    function parentCategory()
    {
        $categories = DB::table('categories')
            ->where('parent_id', 0)
            ->get();
        return $categories;
    }

    function validateAddCategories($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required| max:100',
            'parent_id' => 'required| max:10',
            'slug' => [
                'required',
                Rule::unique('categories')->ignore($request->id),
                'max:100'
            ],
            'sort' => 'max:100',
        ]);
        return $validator;
    }

    function addCategory($request)
    {
        $cat = new CategoryModel();
        $cat->name_en = $request->name;
        $cat->name_jp = $request->name_jp;
        $cat->name_vn = $request->name_vn;
        if ($request->description != null) {
            $cat->description = $request->description;
        }
        $cat->parent_id = $request->parent_id;
        $cat->slug = Str::slug($request->slug, '-');
        $cat->status = $request->status;
        $cat->sort = $request->sort;
        $cat->save();

    }

    function getEditCategories($request){
        $id=$request->id;
        return CategoryModel::find($id);
    }
    function editCategories($request){
        $cat =CategoryModel::find($request->id);
        $cat->name_en = $request->name;
        $cat->name_jp = $request->name_jp;
        $cat->name_vn = $request->name_vn;
        if ($request->description != null) {
            $cat->description = $request->description;
        }
        $cat->parent_id = $request->parent_id;
        $cat->slug = Str::slug($request->slug, '-');
        $cat->status = $request->status;
        $cat->sort = $request->sort;
        $cat->save();
    }
    function deleteCategories($request)
    {
        $id = $request->Id;
        $Categories = CategoryModel::find($id);
        $data=CategoryModel::where('parent_id',$id)->count();
        if ($Categories->parent_id == 0 && $data > 0 ) {
            return 1;
        } else {
            $Categories->delete();
            return 0 ;
        }
    }




    function getNew()
    {
        $query = DB::table('news')
            ->where('news.del_flg', '0')->get();
        return datatables($query)->make(true);
    }


    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateAddParams($request)
    {
        $validator = Validator::make($request->all(), [
            'tittles_vn' => 'required| max:100',
//            'tittles_jp' => 'required| max:100',
//            'tittles_en' => 'required| max:100',
            'slug' => 'required|unique:news| max:100',
            'contents_vn' => 'required',
//            'contents_jp' => 'required',
//            'contents_en' => 'required',
            'new_avatars' => 'mimes:jpeg,jpg,png|required|max:20000' // max 20000kb
        ]);
        return $validator;
    }

    /**
     * @param $request Request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    function validateEditParams($request)
    {
        $validator = Validator::make($request->all(), [
            'tittles_vn' => 'required| max:100',

            'slug' => [
                'required',
                Rule::unique('news')->ignore($request->id),
                'max:100'
            ],
            'contents_vn' => 'required',

               'new_avatars' => 'mimes:jpeg,jpg,png|max:100000' // max 20000kb
        ]);
        return $validator;
    }


    /**
     * @param $request Request
     */
    function addNew($request)
    {

        $new = new NewModel();
        $new->author = Auth::user()->name;
        $new->slug = Str::slug($request->slug, '-');
        $new->tittle_vn = $request->tittles_vn;
        $new->tittle_jp = $request->tittles_jp;
        $new->tittle_en = $request->tittles_en;
        $new->content_vn = $request->contents_vn;
        $new->content_jp = $request->contents_jp;
        $new->content_en = $request->contents_en;
        $new->category = $request->category;
        if (!empty($request->file('new_avatars'))) {
            $file = $request->file('new_avatars');
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $new->avatar = $avatar;
            $request->file('new_avatars')->move('img/upload', $avatar);
            $new->save();
        } else {
            echo "Not Found";
        }
        if (!empty($request->file('m_video'))) {
            $file = $request->file('m_video');
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $new->video = $avatar;
            $request->file('m_video')->move('img/video', $avatar);
            $new->save();
        } else {
            echo "Not Found";
        }
        $new->save();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($new)
            ->log('Add News ');
    }

    /**
     *
     * @param $request Request
     */
    function deleteNew($request)
    {
        $id = $request->Id;
        $new = NewModel::find($id);
        $new->del_flg = 1;
        $new->save();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($new)
            ->log('Delete News with ID ' . $id);
    }

    /**
     * @return string
     */
    function editNewById($id)
    {
        return NewModel::find($id);
    }

    /**
     * @param $request Request
     * @return string
     */
    function editNew($request)
    {
        $id = $request->id;
        $new = NewModel::find($id);
        $new->slug = Str::slug($request->slug, '-');
        $new->tittle_vn = $request->tittles_vn;
        $new->tittle_jp = $request->tittles_jp;
        $new->tittle_en = $request->tittles_en;
        $new->content_vn = $request->contents_vn;
        $new->content_jp = $request->contents_jp;
        $new->content_en = $request->contents_en;
        $new->category = $request->category;
        if (!empty($request->file('new_avatars'))) {
            File::delete('img/upload/' . $new->avatar);
            $new->delete($id);
            $file = $request->file('new_avatars');
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $new->avatar = $avatar;
            $request->file('new_avatars')->move('img/upload', $avatar);
            $new->save();
        } else {
            echo "Not Found";
        }
        if (!empty($request->file('m_video'))) {
            File::delete('img/video/' . $new->video);
            $new->delete($id);
            $file = $request->file('m_video');
            $avatar = Str::uuid() . "." . $file->getClientOriginalExtension();
            $new->video = $avatar;
            $request->file('m_video')->move('img/video', $avatar);
            $new->save();
        } else {
            echo "Not Found";
        }
        $new->save();
        activity()
            ->causedBy(Auth::user())
            ->performedOn($new)
            ->log('Edit News with ID ' . $new->id);
    }

    function hotNews($request)
    {
        $id = $request->id;
        $news = NewModel::find($id);
        $news->status = $request->status;
        $news->save();
    }

    function newsHot()
    {
        $news_hot = DB::table('news')
            ->where('news.del_flg', '0')
            ->orderBy('status', 'Desc')
            ->orderBy('created_at', 'ASC')
            ->paginate(5);
        return $news_hot;
    }


}

