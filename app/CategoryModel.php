<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryModel extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table ="categories";
    protected $fillable = [
        'slug', 'parent_id','name_en','name_jp','name_vn','sort','description','status','created_at','updated_at'
    ];
    public $timestamps= true;

}
